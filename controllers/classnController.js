const controller ={};
const { validationResult } = require('express-validator');

controller.list = (req,res) => {
    req.getConnection((err,conn) =>{
        conn.query('select * from classn;',(err,classntype) =>{
            if(err){
                res.json(err);
            }
            res.render('classn/classnList',{session: req.session,
              data:classntype,
              });
            });
        });
};
module.exports = controller;
