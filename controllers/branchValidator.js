
const { check } = require('express-validator');

exports.addValidator = [check('name',"กรุณาใส่ชื่อโปรแกรมวิชา").not().isEmpty(),
                        check('faculty_id',"กรุณาใส่คณะวิชา").not().isEmpty()
];

 exports.editValidator = [check('name',"กรุณาใส่ชื่อโปรแกรมวิชา").not().isEmpty(),
                         check('faculty_id',"กรุณาใส่คณะวิชา").not().isEmpty()
 ];
