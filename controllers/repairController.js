const controller = {};
const { validationResult } = require('express-validator');
const { default: validator } = require('validator');


controller.repair_add = (req, res) => {
  const { buid } = req.params;
  req.getConnection((err, conn) => {
    conn.query('select * from building ', (err, building) => {
        res.render('repair/repair_add', {
          session: req.session,
          building
        });
    })
  })
};

controller.repair_save = (req, res) => {
  const data = req.body;
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    req.session.errors = errors;
    req.session.success = false;
    res.redirect('/home');
  } else {
    req.session.success = true;
    req.session.topic = "เพิ่มข้อมูลเสร็จแล้ว";
    
    req.getConnection((err, conn) => {
      var bu = [],rm = [];
      if(data.building_id.length > 1){
        for(var i = 0 ; i< data.room_id.length; i++){
          conn.query('select * from building where  name = ?',[data.building_id[i]] ,(err, building) => {
            bu.push({id:building[0].id});
              });
            };
            
              for(var j = 0 ; j< data.room_id.length; j++){
              conn.query('select * from room where number = ?',[data.room_id[j]] , (err, room) => {
                rm.push({id:room[0].id})
            });
          };
          for(var k = 0 ; k< data.room_id.length; k++){
            conn.query('INSERT INTO  repair  (building_id  , room_id ,repair_list ,phone,number )  values(?,?,?,?,?)', [bu[k].id, rm[k].id, data.repair_list[k], data.phone[k], data.number[k]], (err, repairsave) => {
              console.log(err,repairsave);
          });
        };
        res.redirect('/');
      }else{
        conn.query('select * from building where  name = ?',[data.building_id] ,(err, building) => {
          conn.query('select * from room where number = ?',[data.room_id], (err, room) => {
            conn.query('INSERT INTO  repair  (building_id  , room_id ,repair_list ,phone,number )  values(?,?,?,?,?)', [building[0].id, room[0].id, data.repair_list, data.phone, data.number], (err, repairsave) => {
              console.log(err);
              res.redirect('/');
            });
          });
        });
      }
    });
  };
};

controller.repair_confirm = (req, res) => {
  req.getConnection((err, conn) => {
    conn.query('select re.* from repair as re  join  building as bu on re.building_id = bu.id join room as rm on re.room_id = rm.id where re.confirm = 0 ', (err, repair) => {
      res.render('repair/repair_confirm', {
        session: req.session,
        repair
      });
    });
  });
};

controller.repair_saveconfirm = (req, res) => {
  const { id } = req.params;
  req.getConnection((err, conn) => {
    conn.query('UPDATE repair set confirm = 1  where id = ? ', [id], (err, repair) => {
      if (err) {
        res.json(err);
      }
      res.redirect('/repair/repair_date');
    });
  });
};


controller.repair_date = (req, res) => {
  req.getConnection((err, conn) => {
    conn.query('select re.* from repair as re  join  building as bu on re.building_id = bu.id join room as rm on re.room_id = rm.id where re.confirm = 1 ', (err, repair) => {
      res.render('repair/repair_date', {
        session: req.session,
        repair
      });
    });
  });
};

controller.repair_savedate = (req, res) => {
  const { id } = req.params;
  req.getConnection((err, conn) => {
    conn.query('UPDATE repair set  datetime = now()  where id = ? ', [id], (err, repair) => {
      if (err) {
        res.json(err);
      }
      res.redirect('/home');
    });
  });
};
module.exports = controller;
