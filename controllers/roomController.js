const controller ={};
const { validationResult } = require('express-validator');

controller.list = (req,res) => {
  var url = req.params;
  req.getConnection((err,conn) =>{
    conn.query('select * from building where id = ?',[url.buid],(err,building)=>{
      conn.query('select * from room where room.building_id = ?;',[url.buid],(err,room) =>{
        if(err){
          res.json(err);
        }
        res.render('room/roomList',{
          building,room,url,session: req.session
        });
      });
    });
  });
};

controller.add = (req,res) => {
  var url = req.params;
  req.getConnection((err,conn) => {
    conn.query('SELECT * FROM building where id = ?',[url.buid],(err,buildingtype) =>{
      res.render('room/roomAdd',{
        data1:buildingtype,url,
        session: req.session
      });
    });
  });
};

controller.save = (req,res) => {
  var url = req.params;
  const data=req.body;
  const errors = validationResult(req);
  if(!errors.isEmpty()){
    req.session.errors=errors;
    req.session.success=false;
    res.redirect('/room/add/'+url.buid);
  }else{
    req.session.success=true;
    req.session.topic="เพิ่มข้อมูลเสร็จแล้ว";
    req.getConnection((err,conn)=>{
      conn.query('INSERT INTO room set ?',[data],(err,roomtype)=>{
        res.redirect('/room/list/'+url.buid);
      });
    });
  };
};

controller.del = (req,res) => {
  var url = req.params;
  req.getConnection((err,conn)=>{
    conn.query('select * from room where id = ?',[url.roid],(err,room)=>{
      conn.query('SELECT * FROM building where id = ?',[url.buid],(err1,building) =>{
        if(err){
          res.json(err);
        }else if (err1) {
          res.json(err1);
        }
        res.render('room/roomDelete',{
          building,room,url,session: req.session
        });
      });
    });
  });
};

controller.delete = (req,res) => {
  var url = req.params;
  req.getConnection((err,conn)=>{
    conn.query('DELETE FROM room WHERE id = ?',[url.roid],(err,roomtype)=>{
      if(err){
        const  errorss= {errors: [{ value: '', msg: 'การลบไม่ถูกต้อง', param: '', location: '' }]}
        req.session.errors=errorss;
        req.session.success=false;
        res.redirect('/room/del/'+url.buid+'/'+url.roid);
      }else {
        req.session.success=true;
        req.session.topic="ลบข้อมูลเสร็จแล้ว";
        res.redirect('/room/list/'+url.buid);
      }
    });
  });
};

controller.edit = (req,res) => {
  var url = req.params;
  req.getConnection((err,conn)=>{
    conn.query('SELECT * FROM room WHERE id = ?',[url.roid],(err,room)=>{
      conn.query('SELECT * FROM building where id = ?',[url.buid],(err,building) =>{
        res.render('room/roomUpdate',{
          room,building,url,session: req.session
        });
      });
    });
  });
}

controller.update = (req,res) => {
  const errors = validationResult(req);
  var url = req.params;
  const data = req.body;
  if(!errors.isEmpty()){
    req.session.errors=errors;
    req.session.success=false;
    res.redirect('/room/edit/'+url.buid+'/'+url.roid)
  }else{
    req.session.success=true;
    req.session.topic="แก้ไขข้อมูลเสร็จแล้ว";
    req.getConnection((err,conn) => {
      conn.query('UPDATE  room SET ?  WHERE id = ?',[data,url.roid],(err,roomtype) => {
        if(err){
          res.json(err);
        }
        res.redirect('/room');
      });
    });
  }
};

module.exports = controller;
