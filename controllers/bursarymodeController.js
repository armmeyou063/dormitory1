const controller = {};
const {
  validationResult
} = require('express-validator');


controller.list = (req, res) => {
  req.getConnection((err, conn) => {
    conn.query('select s.studentid as stuid ,s.firstname as fname , s.lastname as lname ,b.name as bname , f.name as facname, c.nclass as nclass,s.id as sid from student as s left join branch as b on s.branch_id = b.id left join faculty as f on s.faculty_id = f.id left join classn as c on s.classn_id = c.id left join bursary as bu on s.bursary_id = bu.id where bu.id = 1;', (err, classn1) => {
      conn.query('select s.studentid as stuid ,s.firstname as fname , s.lastname as lname ,b.name as bname , f.name as facname, c.nclass as nclass,s.id as sid from student as s left join branch as b on s.branch_id = b.id left join faculty as f on s.faculty_id = f.id left join classn as c on s.classn_id = c.id left join bursary as bu on s.bursary_id = bu.id where bu.id = 2;', (err, classn2) => {
        conn.query('select s.studentid as stuid ,s.firstname as fname , s.lastname as lname ,b.name as bname , f.name as facname, c.nclass as nclass,s.id as sid from student as s left join branch as b on s.branch_id = b.id left join faculty as f on s.faculty_id = f.id left join classn as c on s.classn_id = c.id left join bursary as bu on s.bursary_id = bu.id where bu.id = 4;', (err, classn3) => {
          conn.query('select s.studentid as stuid ,s.firstname as fname , s.lastname as lname ,b.name as bname , f.name as facname, c.nclass as nclass,s.id as sid from student as s left join branch as b on s.branch_id = b.id left join faculty as f on s.faculty_id = f.id left join classn as c on s.classn_id = c.id left join bursary as bu on s.bursary_id = bu.id where bu.id = 6;', (err, classn4) => {

              if (err) {
                res.json(err);
              }
              res.render('bursarymode/bursarymodeList', { //หาหน้า views ต้องใส่ / ด้วยเพราะมันหนาใน view ไม่เจอ
                data1: classn1,
                data2: classn2,
                data3: classn3,
                data4: classn4,
                session: req.session
              });
            });
          });
        });
      });
  });
};


controller.report = (req, res) => {
  const {
    id
  } = req.params;
  console.log(id);
  req.getConnection((err, conn) => {
    conn.query('select s.id as sid ,s.firstname as fname ,s.lastname as lname , s.nickname as nname , s.numberid as snum ,br.name as brname ,bur.name as burname,tr.name as trname,s.studentid as stid, s.phone as sphone,DATE_FORMAT(birthday, "วันที่ %d/%m/%Y")birthday,DATE_FORMAT(datein, "วันที่ %d/%m/%Y")datein, s.age as age ,s.bloodtype as bloodtype,s.talent as talent, s.phone as phone ,s.numberhome as numberhome,s.moo as moo,s.tombon as tombon ,s.aumper as aumper ,s.province as province ,s.nfather as nfather ,s.lfather as lfather, s.postalcode as postalcode,bu.name as buname, r.number as number,f.name as facname,c.nclass as nclass,s.nmother as nmother,s.lmother as lmother,sta.name as staname, s.careerfather as careerfather,s.careermother as careermother,s.brother as brother,s.studybro as studybro,s.careerbro as careerbro,sup.name as supname from student as s left join branch as br on s.branch_id = br.id left join bursary as bur on s.bursary_id = bur.id left join tribe as tr on s.tribe_id = tr.id left join support as sup on s.support_id = sup.id left join status as sta on s.status_id = sta.id left join room as r on s.room_id = r.id left join building as bu on s.building_id = bu.id left join faculty as f on s.faculty_id = f.id left join classn as c on s.classn_id = c.id left join tanant  as t on t.room_id = t.id where s.id = ?', [id], (err, bursarymodeList) => {
      if (err) {
        res.json(err);
      }
      res.render('bursarymode/bursarymodeReport', {
        session: req.session,
        data: bursarymodeList[0]
      });
    });
  });
};





module.exports = controller;
