
const { check } = require('express-validator');

exports.addValidator = [check('number',"กรุณาใส่หมายเลขห้องพัก").not().isEmpty(),
                        check('building_id',"กรุณาใส่ชื่อตึก").not().isEmpty()
];

 exports.editValidator = [check('number',"กรุณาใส่หมายเลขห้องพัก").not().isEmpty(),
                         check('building_id',"กรุณาใส่ชื่อตึก").not().isEmpty()
 ];
