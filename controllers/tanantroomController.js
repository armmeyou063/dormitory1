const controller = {};
const { validationResult } = require('express-validator');
const { v4: uuidv4 } = require('uuid');
const fs = require('fs');
//-------------------------------------------------------------------------------------------------------
//ส่วนแสดงข้อมูลหลังจากรอกข้อมูลจอง
//-------------------------------------------------------------------------------------------------------
controller.list = (req, res) => {
  req.getConnection((err, conn) => {
    conn.query(' select st.id as stid, tn.id as tnid, firstname,lastname,studentid,b.name as branch,bu.name as building,rm.number as roomnumber from tanant as tn join student st on tn.student_id = st.id join branch as b  on st.branch_id = b.id join building as bu on tn.building_id = bu.id join room as rm on tn.room_id = rm.id where confirm = 0  and datein is null;', (err, jong) => {
      res.render('tanantroom/tanantroomList', {
        session: req.session,
        jong: jong,
      });
    });
  });
};

//-------------------------------------------------------------------------------------------------------
//ส่วนแสดงข้อมูลรายละเอียดของนศ.
//-------------------------------------------------------------------------------------------------------
controller.report = (req, res) => {
  const { stid } = req.params;
  req.getConnection((err, conn) => {
    conn.query('select student.* from student join branch on student.branch_id = branch.id join  faculty on student.faculty_id = faculty.id join bursary on student.bursary_id = bursary.id join tribe on student.tribe_id = tribe.id where student.id = ?; ', [stid], (err, student) => {
      res.render('tanantroom/tanantreport', {
        session: req.session,
        student
      });
    });
  });
};

//-------------------------------------------------------------------------------------------------------
//ส่วนแสดงข้อมูลเกี่ยวกับสัญญา
//-------------------------------------------------------------------------------------------------------
controller.promise = (req, res) => {
  const { stid } = req.params;
  req.getConnection((err, conn) => {
    conn.query('select * from promise ;', (err, promise) => {
      res.render('tanantroom/tanantpromise', {
        session: req.session,
        promise, stid
      });
    });
  });
};

controller.promise_save = (req, res) => {
  const data = req.body;
  const { stid } = req.params;
  req.getConnection((err, conn) => {
    if (req.files) {
      var file = req.files.filename;
      var type = file.name.split(".");
      var filename = uuidv4() + "." + type[type.length - 1];
      data.filename = filename;
      file.mv("./public/promise/" + filename, function (err) {
        if (err) { console.log(err) }
      });
      conn.query('INSERT INTO promise set student_id = ? ,filename = ?', [stid, data.filename], (err, promise) => {
        if (err) {
          res.json(err);
        }
        res.redirect('/tanantcomin');
      });
    } else {
      conn.query('INSERT INTO promise set student_id = ? ,filename = ?', [stid, data.filename], (err, promise) => {
        console.log(err);
        if (err) {
          res.json(err);
        }
        res.redirect('/tanantcomin');
      });
    }
  });
};
//-------------------------------------------------------------------------------------------------------
//ส่วนแสดงข้อมูลยืนยันการจอง
//-------------------------------------------------------------------------------------------------------
controller.jonglist = (req, res) => {
  const { stid } = req.params;
  const { buid } = req.params;
  console.log(buid);
  req.getConnection((err, conn) => {
    conn.query('select * from tanant as ta where building_id = ?;', [buid], (err, building) => {
      conn.query(' select bu.id as buid ,st.id as stid,tn.id as tnid , firstname,lastname,studentid,b.name as branch,bu.name as building,rm.number as roomnumber from tanant as tn join student st on tn.student_id = st.id join branch as b  on st.branch_id = b.id join building as bu on tn.building_id = bu.id join room as rm on tn.room_id = rm.id where confirm = 1  and datein is null;', (err, jong) => {
        console.log(err);
        res.render('tanantroom/tanantcomin', {
          session: req.session,
          jong, stid, buid, building
        });

      });
    });
  });
};


//-------------------------------------------------------------------------------------------------------
//ส่วนกดยืนยันการจอง
//-------------------------------------------------------------------------------------------------------
controller.updatejong = (req, res) => {
  const errors = validationResult(req);
  const { id } = req.params;
  if (!errors.isEmpty()) {
    req.session.errors = errors;
    req.session.success = false;
    res.redirect('/tanantroom')
  } else {
    req.session.success = true;
    req.session.topic = "อัปเดตข้อมูลเสร็จแล้ว";
    req.getConnection((err, conn) => {
      conn.query('UPDATE tanant set confirm = 1 where id = ? ', [id], (err, tanantupdate) => {
        if (err) {
          res.json(err);
        }
        res.redirect('/tanantcomin');
      });
    });
  };
};


//-------------------------------------------------------------------------------------------------------
//ส่วนกดยืนยันการเข้าหอพัก
//-------------------------------------------------------------------------------------------------------
controller.updatecomin = (req, res) => {
  const errors = validationResult(req);
  const { tnid } = req.params;
  const { buid } = req.params;
  console.log(buid);
  if (!errors.isEmpty()) {
    req.session.errors = errors;
    req.session.success = false;
    res.redirect('/tanantroom')
  } else {
    req.session.success = true;
    req.session.topic = "อัปเดตข้อมูลเสร็จแล้ว";
    req.getConnection((err, conn) => {
      conn.query('UPDATE tanant set datein = now() where id = ? ', [tnid], (err, tanantupdate) => {
        if (err) {
          res.json(err);
        }
      });
      conn.query('select student_id , building_id , room_id , datein as date from tanant where id = ?', [tnid], (err, tanant) => {
        console.log(err);
        conn.query('insert into current set ?', [tanant[0]], (err, current) => {
          console.log(err);
          res.redirect('/student/studentlist/' + buid[0]);
        })
      })
    });
  };
};

controller.delete = (req, res) => {
  const { stid } = req.params;
  console.log(stid);
  const { id } = req.params;
  console.log(id);
  req.getConnection((err, conn) => {
    // conn.query('select id from student where id  = ?;', [stid], (err, student1) => {
    conn.query('DELETE FROM tanant WHERE id= ?', [id], (err, tanant) => {
      conn.query('DELETE FROM student WHERE id= ?', [stid], (err, student) => {
        if (err) {
          const errorss = {
            errors: [{
              value: '',
              msg: 'การลบไม่ถูกต้อง',
              param: '',
              location: ''
            }]
          }
          req.session.errors = errorss;
          req.session.success = false;
        } else {
          req.session.success = true;
          req.session.topic = "ยกเลิกการจองสำเร็จ";
        }
        res.redirect('/tanantroom');
      });
      // });
    });
  });
};




controller.del = (req, res) => {
  const {
    id
  } = req.params;
  req.getConnection((err, conn) => {
    conn.query('select st.id as stid, st.firstname as firstname, st.lastname as lastname, st.studentid as studentid , br.name as brname from student as st left join branch as br on st.branch_id = br.id where st.id = ?', [id], (err, student) => {
      conn.query('select * from branch', [id], (err1, branch) => {
        if (err) {
          res.json(err);
        }
        if (err1) {
          res.json(err);
        }
        res.render('tanantroom/tanantroomDelete', {
          session: req.session,
          data: student[0],
          data1: branch[0]
        });
      });
    });
  });
};



module.exports = controller;
