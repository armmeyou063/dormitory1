const controller = {};
const { v4: uuidv4 } = require('uuid');
const fs = require('fs');
const { validationResult } = require('express-validator');
const { default: validator } = require('validator');


controller.log = (req, res) => {
  res.render('login', {
    session: req.session
  });
};


controller.login = function (req, res) {
  if (req.body.username == 'admin' && req.body.password == 'admin') {
    req.session.user = req.body.username;
    res.render('home', {
      session: req.session
    });
  } else {
    res.render('login', {
      session: req.session
    });
  };
};

controller.home = function (req, res) {
  if (req.session.user) {
    res.render('home', {
      session: req.session
    });
  } else {
    res.redirect('/');
  }
};

controller.homeelec = function (req, res) {
  if (req.session.user) {
    res.render('homeelecwater', {
      session: req.session
    });
  } else {
    res.redirect('/');
  }
};

controller.logout = (req, res) => {
  if (!req.session.user) {
    res.redirect('/login');
  } else {
    req.session.destroy(function (errors) {
      res.redirect('/login');
    })
  }
};




//-------------------------------------------------------------------------------------------------------
//ส่วนแสดงข้อมูลหน้าการเลือกหอพัก
//-------------------------------------------------------------------------------------------------------
controller.Dorm = (req, res) => {
  const { dormid } = req.params;
  var data = req.body;
  if (data.get) {
    req.session.get = data.get;
  }
  req.getConnection((err, conn) => {
    conn.query('select bu.*, sum(rm.peoplenumber) as peoplenumber from building as bu join room as rm  on rm.building_id = bu.id  group by bu.id;', (err, build) => {
      conn.query('SELECT * FROM rule', (err, rule) => {
        conn.query(' select count(cu.id) as cuid,building.id as buid from  (select bu.* , rm.id as rmid from room as rm  join building as bu on rm.building_id = bu.id) as building left join current as cu  on building.rmid = cu.room_id  group by building.id;', (err, stbu) => {
          res.render('Index/dorm', {
            session: req.session,
            build,
            stbu,
            rule,
            dormid
          });
        });
      });
    });
  });
};


//-------------------------------------------------------------------------------------------------------
//ส่วนแสดงข้อมูลหน้าการเลือกห้องพัก
//-------------------------------------------------------------------------------------------------------
controller.Room = (req, res) => {
  const { buid } = req.params;
  req.getConnection((err, conn) => {
    conn.query('select * from building where id = ?;', [buid], (err, bu) => {
      conn.query('select price from building join paybuild on paybuild.building_id = building.id where building.id = ?;',[buid], (err, paybuild) => {
        conn.query('select * from room where building_id = ?;', [buid], (err, rm) => {
          conn.query('  select room.id as rmid, count(cu.id) as count from (select rm.* from room as rm join building bu on rm.building_id = bu.id) as room left join current as cu on cu.room_id = room.id where room.building_id = ?  group by room.id;', [buid], (err, strm) => {
            res.render('Index/room', {
              session: req.session,
              strm,
              rm,
              bu,
              buid,
              paybuild
            });
          });
        });
      });
    });
  });
};


//-------------------------------------------------------------------------------------------------------
//ส่วนแสดงการกรอกข้อมูลการจอง
//-------------------------------------------------------------------------------------------------------
controller.index = (req, res) => {
  const { rmid } = req.params;
  const { buid } = req.params;
  req.getConnection((err, conn) => {
    conn.query('SELECT * FROM provinces order by name_th', (err, provinces) => {
      conn.query('SELECT * FROM bursary', (err, bursary) => {
        conn.query('SELECT * FROM tribe', (err, tribe) => {
          conn.query('SELECT * FROM status', (err, status) => {
            conn.query('SELECT * FROM support', (err, support) => {
              conn.query('SELECT * FROM room where id = ?', [rmid], (err, room) => {
                conn.query('SELECT * FROM building where id = ?', [buid], (err, building) => {
                  conn.query('SELECT * FROM faculty', (err, faculty) => {
                    conn.query('SELECT * FROM class', (err, classn) => {
                      res.render('Index/index', {
                        provinces, bursary, tribe, status, support, room, building, faculty,
                        classn, rmid, buid, session: req.session
                      });
                    });
                  });
                });
              });
            });
          });
        });
      });
    });
  });
};


//-------------------------------------------------------------------------------------------------------
//ส่วนบันทึกข้อมูลการจอง
//-------------------------------------------------------------------------------------------------------
controller.save = (req, res) => {
  var { rmid } = req.params;
  var { buid } = req.params;
  var data = req.body;
  var nbid = data.numberid[0] + data.numberid[1] + data.numberid[2] + data.numberid[3] + data.numberid[4] + data.numberid[5] + data.numberid[6] + data.numberid[7] + data.numberid[8] + data.numberid[9] + data.numberid[10] + data.numberid[11] + data.numberid[12];
  data.numberid = nbid;
  req.getConnection((err, conn) => {
    if (req.files) {
      var file = req.files.photo;
      var type = file.name.split(".");
      var filename = uuidv4() + "." + type[type.length - 1];
      data.photo = filename;
      file.mv("./public/pigture/" + filename, function (err) {
        if (err) { console.log(err) }
      });
      conn.query('INSERT INTO  student set  firstname = ?, lastname = ? ,nickname = ? , numberid = ? , branch_id = ? ,studentid = ? ,birthday = ? , bloodtype = ? , talent = ? , bursary_id = ? ,tribe_id = ? , phone = ? , address = ? , districts = ? , amphures = ? , province = ? ,pos = ? , namefather = ? ,namemother = ? , jobfather = ?, jobmother = ? , brothernumber = ? ,studynumber = ? , jobnumber = ?  ,chan = ? ,address1 = ?, province1 = ? , amphures1 = ? , districts1 = ? , pos1 = ? , facebook = ? , uprakaraother = ? , uprakara = ? , statusother = ? , statusparent = ? , agemother = ? ,agefather = ? ,photo = ? ,faculty_id = ?  ;',
      [data.firstname, data.lastname ,data.nickname, data.numberid, data.branch_id, data.studentid , data.birthday , data.bloodtype , data.talent , data.bursary_id ,data.tribe_id, data.phone , data.address , data.districts , data.amphures, data.province , data.pos , data.namefather , data.namemother , data.jobfather , data.jobmother , data.brothernumber , data.studynumber , data.jobnumber  , data.chan , data.address1 , data.province1 , data.amphures1 , data.districts1 , data.pos1 , data.facebook , data.uprakaraother , data.uprakara , data.statusother , data.statusparent , data.agemother , data.agefather, data.photo , data.faculty_id], (err, student) => {
        if (err) {
          res.json(err)
        };
      });
      conn.query('select * from student where numberid = ?', [data.numberid], (err, numberid) => {
        conn.query('INSERT INTO tanant  (student_id , room_id,building_id) value(?,?,?)', [numberid[0].id, rmid, buid], (err, tanant) => {
          console.log(err,tanant);
          if (err) {
            res.json(err)
          }
          res.redirect('/');
        })
      })
    } else {
      console.log("N");
      conn.query('INSERT INTO  student set firstname = ?, lastname = ? ,nickname = ? , numberid = ? , branch_id = ? ,studentid = ? ,birthday = ? , bloodtype = ? , talent = ? , bursary_id = ? ,tribe_id = ? , phone = ? , address = ? , districts = ? , amphures = ? , province = ? ,pos = ? , namefather = ? ,namemother = ? , jobfather = ?, jobmother = ? , brothernumber = ? ,studynumber = ? , jobnumber = ?  ,chan = ? ,address1 = ?, province1 = ? , amphures1 = ? , districts1 = ? , pos1 = ? , facebook = ? , uprakaraother = ? , uprakara = ? , statusother = ? , statusparent = ? , agemother = ? ,agefather = ? ,photo = ? ,faculty_id = ?  ;',
      [data.firstname, data.lastname ,data.nickname, data.numberid, data.branch_id, data.studentid , data.birthday , data.bloodtype , data.talent , data.bursary_id ,data.tribe_id, data.phone , data.address , data.districts , data.amphures, data.province , data.pos , data.namefather , data.namemother , data.jobfather , data.jobmother , data.brothernumber , data.studynumber , data.jobnumber  , data.chan , data.address1 , data.province1 , data.amphures1 , data.districts1 , data.pos1 , data.facebook , data.uprakaraother , data.uprakara , data.statusother , data.statusparent , data.agemother , data.agefather, data.photo , data.faculty_id], (err, student) => {
        console.log(student,err);
        if (err) {
          res.json(err)
        }
      });
      conn.query('select * from student where numberid = ?', [data.numberid], (err, numberid) => {
        conn.query('INSERT INTO tanant (student_id , room_id,building_id ) values (?,?,?)', [numberid[0].id, rmid, buid], (err, tanant) => {
          if (err) {
            res.json(err)
          }
          res.redirect('/');
        })
      });
    }
  });
}

// controller for ajax

controller.getbranch = (req, res) => {
  var url = req.params;
  req.getConnection((err, conn) => {
    conn.query('select * from branch where faculty_id = ?', [url.faid], (err, brach) => {
      res.send(
        brach
      );
    });
  });
}

controller.getamphures = (req, res) => {
  var url = req.params;
  req.getConnection((err, conn) => {
    conn.query('select * from amphures where province_id = ? order by name_th', [url.prid], (err, amphures) => {
      res.send(
        amphures
      );
    });
  });
}

controller.getdistricts = (req, res) => {
  var url = req.params;
  req.getConnection((err, conn) => {
    conn.query('select * from districts where amphure_id = ? order by name_th', [url.amid], (err, districts) => {
      res.send(
        districts
      );
    });
  });
}

controller.getzipcode = (req, res) => {
  var url = req.params;
  req.getConnection((err, conn) => {
    conn.query('select * from districts where id = ?', [url.diid], (err, zipcode) => {
      res.send(
        zipcode
      );
    });
  });
}

controller.getcontactamphures = (req, res) => {
  var url = req.params;
  req.getConnection((err, conn) => {
    conn.query('select * from amphures where province_id = ? order by name_th', [url.prid], (err, amphures) => {
      res.send(
        amphures
      );
    });
  });
}

controller.getcontactdistricts = (req, res) => {
  var url = req.params;
  req.getConnection((err, conn) => {
    conn.query('select * from districts where amphure_id = ? order by name_th', [url.amid], (err, districts) => {
      res.send(
        districts
      );
    });
  });
}

controller.getcontactzipcode = (req, res) => {
  var url = req.params;
  req.getConnection((err, conn) => {
    conn.query('select * from districts where id = ?', [url.diid], (err, zipcode) => {
      res.send(
        zipcode
      );
    });
  });
}

module.exports = controller;
