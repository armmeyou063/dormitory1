const controller = {};
const {
  validationResult
} = require('express-validator');


controller.list = (req, res) => {
  req.getConnection((err, conn) => {
    conn.query('select bu.name as namebu,rm.number as number,st.id idst, studentid, firstname, lastname,bn.name namebn, fac.id as isfac,fac.name as facname from student st left join faculty fac on st.faculty_id = fac.id left join branch bn on st.branch_id = bn.id left join building as bu on st.building_id = bu.id left join room as rm on st.room_id = rm.id ; ', (err, st) => {
      conn.query('select * from faculty;', (err, fac) => {
        if (err) {
          res.json(err);
        }
        res.render('facultymode/facultymodeList', { //หาหน้า views ต้องใส่ / ด้วยเพราะมันหนาใน view ไม่เจอ
          st: st,
          fac: fac,
          session: req.session
        });
      });
    });
  });
};


controller.report = (req, res) => {
  const {
    id
  } = req.params;
  req.getConnection((err, conn) => {
    conn.query('select s.id as sid ,s.firstname as fname ,s.lastname as lname , s.nickname as nname , s.numberid as snum ,br.name as brname ,bur.name as burname,tr.name as trname,s.studentid as stid, s.phone as sphone,DATE_FORMAT(birthday, "วันที่ %d/%m/%Y")birthday,DATE_FORMAT(datein, "วันที่ %d/%m/%Y")datein, s.age as age ,s.bloodtype as bloodtype,s.talent as talent, s.phone as phone ,s.numberhome as numberhome,s.moo as moo,s.tombon as tombon ,s.aumper as aumper ,s.province as province ,s.nfather as nfather ,s.lfather as lfather, s.postalcode as postalcode,bu.name as buname, r.number as number,f.name as facname,c.nclass as nclass,s.nmother as nmother,s.lmother as lmother,sta.name as staname, s.careerfather as careerfather,s.careermother as careermother,s.brother as brother,s.studybro as studybro,s.careerbro as careerbro,sup.name as supname from student as s left join branch as br on s.branch_id = br.id left join bursary as bur on s.bursary_id = bur.id left join tribe as tr on s.tribe_id = tr.id left join support as sup on s.support_id = sup.id left join status as sta on s.status_id = sta.id left join room as r on s.room_id = r.id left join building as bu on s.building_id = bu.id left join faculty as f on s.faculty_id = f.id left join classn as c on s.classn_id = c.id left join tanant  as t on t.room_id = t.id where s.id = ?', [id], (err, classmodeList) => {
      if (err) {
        res.json(err);
      }
      res.render('facultymode/facultymodeReport', {
        session: req.session,
        data: classmodeList[0]
      });
    });
  });
};





module.exports = controller;
