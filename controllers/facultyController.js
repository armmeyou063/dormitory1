const controller ={};
const { validationResult } = require('express-validator');

controller.list = (req,res) => {
    req.getConnection((err,conn) =>{
        conn.query('select * from faculty;',(err,facultytype) =>{
            if(err){
                res.json(err);
            }
            res.render('faculty/facultyList',{session: req.session,
              data:facultytype,
              });
            });
        });
};
controller.save = (req,res) => {
    const data=req.body;
       const errors = validationResult(req);
        if(!errors.isEmpty()){
            req.session.errors=errors;
            req.session.success=false;
            res.redirect('/faculty/add');
        }else{
            req.session.success=true;
            req.session.topic="เพิ่มข้อมูลเสร็จแล้ว";
            req.getConnection((err,conn)=>{
            conn.query('INSERT INTO faculty set ?',[data],(err,facultytype)=>{
            res.redirect('/faculty');
            });
        });
    };
};
controller.del = (req,res) => {
    const { id } = req.params;
    req.getConnection((err,conn)=>{
        conn.query('select * from faculty HAVING id = ?',[id],(err,facultytype)=>{
          if(err){
              res.json(err);
          }
          res.render('faculty/facultyDelete',{
            session: req.session,
            data:facultytype[0]
          });
      });
  });
};
controller.delete = (req,res) => {
    const { id } = req.params;
    req.getConnection((err,conn)=>{
        conn.query('DELETE FROM faculty WHERE id= ?',[id],(err,facultytype)=>{
            if(err){
              const  errorss= {errors: [{ value: '', msg: 'การลบไม่ถูกต้อง', param: '', location: '' }]}
              req.session.errors=errorss;
              req.session.success=false;
            }else {
                req.session.success=true;
                req.session.topic="ลบข้อมูลเสร็จแล้ว";
            }
            console.log(facultytype);
            res.redirect('/faculty');
        });
    });
};
controller.edit = (req,res) => {
    const { id } = req.params;
        req.getConnection((err,conn)=>{
                conn.query('SELECT * FROM faculty WHERE id= ?',[id],(err,facultytype)=>{
                        res.render('faculty/facultyUpdate',{
                          session: req.session,
                          data1:facultytype[0],
                        });
                });
            });
}
controller.update = (req,res) => {
    const errors = validationResult(req);
    const { id } = req.params;
    const data = req.body;
        if(!errors.isEmpty()){
            req.session.errors=errors;
            req.session.success=false;
            req.getConnection((err,conn)=>{
                conn.query('SELECT * FROM faculty WHERE id= ?',[id],(err,facultytype)=>{
                res.render('faculty/facultyUpdate',{
                  session: req.session,
                  data1:facultytype[0],
                });
              });
            });
        }else{
            req.session.success=true;
            req.session.topic="แก้ไขข้อมูลเสร็จแล้ว";
            req.getConnection((err,conn) => {
                conn.query('UPDATE  faculty SET ?  WHERE id = ?',[data,id],(err,facultytype) => {
                  if(err){
                      res.json(err);
                  }
               res.redirect('/faculty');
               });
             });
        }
};

controller.add = (req,res) => {
    req.getConnection((err,conn) => {
      conn.query('SELECT * FROM faculty',(err,facultytype) =>{
      res.render('faculty/facultyAdd',{
        data:facultytype,
        session: req.session
      });
    });
  });
};

module.exports = controller;
