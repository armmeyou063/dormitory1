const controller = {};
const {
  validationResult
} = require('express-validator');

controller.list = (req, res) => {
  req.getConnection((err, conn) => {
      conn.query('select st.id as idst,bu.name as namebu, rm.number as number, st.firstname as firstname,st.lastname as lastname,bn.name as namebn, DATE_FORMAT(datein, "วันที่ %d/%m/%Y")dayin, st.id as stid,bu.id as buid, rm.id as rmid from student  as st left join building as bu on st.building_id = bu.id left join branch as bn on st.branch_id = bn.id left join tanant as tn on tn.student_id = st.id left join room as rm on st.room_id = rm.id left join tanantout as tnout on tnout.student_id = st.id where dateout is null;', (err, tn) => {
          conn.query('select * from building;', (err1, bu) => {
          res.render('tanant/tanantList', {
              session: req.session,
              tn: tn,
              bu: bu
            });
          });
      });
  });
};

controller.listTanantBuilding = (req, res) => {
  const {id} = req.params;
  req.getConnection((err, conn) => {
    conn.query('select * from building;', (err2, bu) => {
      conn.query('select firstname,building_id from student st left join building bu on st.building_id = bu.id;', (err2, stbu) => {
        conn.query('select bu.id idbu, bu.name namebu, number, peoplenumber, rm.building_id rmidbu from building bu left join room rm on rm.building_id = bu.id;', (err2, burm) => {
          res.render('tanant/listTanantBuilding', {
            session: req.session,
            bu: bu,
            stbu: stbu,
            burm: burm,
            tanantroomid:id
          });
        });
      });
    });
  });
};

controller.ListBuilding = (req, res) => {
  req.getConnection((err, conn) => {
    conn.query('select bu.id idbu, rm.id idrm, name,  number, building_id, rm.peoplenumber nump from building bu left join room rm on rm.building_id = bu.id where bu.id = 8;', (err1, tananttype1) => {
      conn.query('select bu.id idbu, rm.id idrm, name,  number, building_id, rm.peoplenumber nump from building bu left join room rm on rm.building_id = bu.id where bu.id = 9 ;', (err4, bu2) => {
        conn.query('select bu.id idbu, rm.id idrm, name,  number, building_id, rm.peoplenumber nump from building bu left join room rm on rm.building_id = bu.id where bu.id = 10 ;', (err5, bu3) => {
          conn.query('select bu.id idbu, rm.id idrm, name,  number, building_id, rm.peoplenumber nump from building bu left join room rm on rm.building_id = bu.id where bu.id = 11 ;', (err6, bu4) => {
            conn.query('select bu.id idbu, rm.id idrm, name,  number, building_id, rm.peoplenumber nump from building bu left join room rm on rm.building_id = bu.id where bu.id = 12 ;', (err7, bu5) => {
              conn.query('select bu.id idbu, rm.id idrm, name,  number, building_id, rm.peoplenumber nump from building bu left join room rm on rm.building_id = bu.id where bu.id = 13 ;', (err8, bu6) => {
                conn.query('select bu.id idbu, rm.id idrm, name,  number, building_id, rm.peoplenumber nump from building bu left join room rm on rm.building_id = bu.id where bu.id = 14 ;', (err9, bu7) => {
                  conn.query('select * from building group by name;', (err2, tananttype2) => {
                    res.render('tanant/tanantBuilding', {
                      session: req.session,
                      data: tananttype1,
                      data1: tananttype2,
                      data2: bu2,
                      data3: bu3,
                      data4: bu4,
                      data5: bu5,
                      data6: bu6,
                      data7: bu7,
                    });
                  });
                });
              });
            });
          });
        });
      });
    });
  });
};

controller.ListBuildingSee = (req, res) => {
  const {id} = req.params;
  req.getConnection((err, conn) => {
    conn.query('select rm.id idrm, rm.building_id idbu ,bu.name namebu, number, rm.peoplenumber nump from building bu left join room rm on rm.building_id = bu.id where bu.id = 8 order by rm.id ;', [id], (err3, bu1) => {
      conn.query('select rm.id idrm, rm.building_id idbu ,bu.name namebu, number, rm.peoplenumber nump from building bu left join room rm on rm.building_id = bu.id where bu.id = 9 ;', [id], (err4, bu2) => {
        conn.query('select rm.id idrm, rm.building_id idbu ,bu.name namebu, number, rm.peoplenumber nump from building bu left join room rm on rm.building_id = bu.id where bu.id = 10 ;', [id], (err5, bu3) => {
          conn.query('select rm.id idrm, rm.building_id idbu ,bu.name namebu, number, rm.peoplenumber nump from building bu left join room rm on rm.building_id = bu.id where bu.id = 11 ;', [id], (err6, bu4) => {
            conn.query('select rm.id idrm, rm.building_id idbu ,bu.name namebu, number, rm.peoplenumber nump from building bu left join room rm on rm.building_id = bu.id where bu.id  = 12 ;', [id], (err7, bu5) => {
              conn.query('select rm.id idrm, rm.building_id idbu ,bu.name namebu, number, rm.peoplenumber nump from building bu left join room rm on rm.building_id = bu.id where bu.id = 13 ;', [id], (err8, bu6) => {
                conn.query('select rm.id idrm, rm.building_id idbu ,bu.name namebu, number, rm.peoplenumber nump from building bu left join room rm on rm.building_id = bu.id where bu.id = 14 ;', [id], (err9, bu7) => {
                  res.render('tanant/tanantBuildingSee', {
                    session: req.session,
                    data1: bu1,
                    data2: bu2,
                    data3: bu3,
                    data4: bu4,
                    data5: bu5,
                    data6: bu6,
                    data7: bu7,
                    data: id
                  });
                });
              });
            });
          });
        });
      });
    });
  });
};


controller.del = (req, res) => {
  const {
    id
  } = req.params;
  const {
    strm_id
  } = req.params;
  const {
    numrm
  } = req.params;
  req.getConnection((err, conn) => {
    conn.query('select st.id idst, rm.id idrm, bu.name namebu, rm.number numberrm, firstname, lastname, bn.name namebn, st.datein dayins , DATE_FORMAT(datein, "วันที่ %d/%m/%Y")dayin from student st left join room rm on st.room_id = rm.id left join branch bn on st.branch_id = bn.id left join building bu on rm.building_id = bu.id left join tanantout tnout on tnout.student_id = st.id where st.id = ? and tnout.dateout is null;', [id], (err, aaaaa) => {
      conn.query('select * from tanant HAVING id = ?', [id], (err, tananttype) => {
        conn.query('select * from student', (err1, studenttype) => {
          conn.query('select * from room', (err2, roomtype) => {
            conn.query('select * from building', (err3, buildingtype) => {
              if (err) {
                res.json(err);
              }
              if (err1) {
                res.json(err);
              }
              if (err2) {
                res.json(err);
              }
              if (err3) {
                res.json(err);
              }
              res.render('tanant/tanantDelete', {
                session: req.session,
                data1: tananttype[0],
                data2: studenttype[0],
                data3: roomtype[0],
                data4: buildingtype[0],
                data: aaaaa[0],
                datarm: numrm,
                datastrm: strm_id
              });
            });
          });
        });
      });
    });
  });
};


controller.delete = (req, res) => {
  const data = req.body;
  const {
    id
  } = req.params;
  const {
    strm_id
  } = req.params;
  const {
    numrm
  } = req.params;
console.log(id);
  req.getConnection((err, conn) => {
    conn.query('INSERT INTO tanantout set ?', [data], (err, tan) => {
      conn.query('UPDATE student set room_id = null , building_id = null where id = ?', [id], (err, xx) => {
        conn.query('DELETE FROM tanant where student_id = ?', [id], (err, xxx) => {
        res.redirect('/tanant');
        });
      });
    });
  });
};

controller.addBuilding = (req, res) => {
  const {
    id
  } = req.params;
    const {
tanantroomid
    } = req.params;
  req.getConnection((err, conn) => {
    conn.query('select * from building;', [id], (err3, bu) => {
      conn.query('select * from room;', [id], (err3, rm) => {
        conn.query('select bu.id idbu ,st.room_id stidrm ,st.id idst, rm.id idrm, number from student st left join room rm on st.room_id = rm.id left join building bu on st.building_id = bu.id;', [id], (err3, strm) => {
          res.render('tanant/tanantAddBuilding', {
            session: req.session,
            strm: strm,
            rm: rm,
            bu: bu,
            id: id,
            tanantroomid:tanantroomid
          });
        });
      });
    });
  });
};

controller.ListEditBuilding = (req, res) => {
  const {
    strm_id
  } = req.params;
  const {
    idst
  } = req.params;
  const {
    numrm
  } = req.params;
  req.getConnection((err, conn) => {
    res.render('tanant/tanantEditBuilding', {
      session: req.session,
      datast: idst,
      datastrm: strm_id,
      da: numrm
    });
  });
};

controller.addEditBuilding = (req, res) => {
  const {
    id
  } = req.params;
  const {
    idst
  } = req.params;
  const {
    strm_id
  } = req.params;
  const {
    numrm
  } = req.params;
  req.getConnection((err, conn) => {
    conn.query('select rm.id idrm, rm.building_id idbu ,bu.name namebu, number, rm.peoplenumber nump from building bu left join room rm on rm.building_id = bu.id where bu.id = 8 order by rm.id ;', [id], (err3, bu1) => {
      conn.query('select rm.id idrm, rm.building_id idbu ,bu.name namebu, number, rm.peoplenumber nump from building bu left join room rm on rm.building_id = bu.id where bu.id = 9 ;', [id], (err4, bu2) => {
        conn.query('select rm.id idrm, rm.building_id idbu ,bu.name namebu, number, rm.peoplenumber nump from building bu left join room rm on rm.building_id = bu.id where bu.id = 10 ;', [id], (err5, bu3) => {
          conn.query('select rm.id idrm, rm.building_id idbu ,bu.name namebu, number, rm.peoplenumber nump from building bu left join room rm on rm.building_id = bu.id where bu.id = 11 ;', [id], (err6, bu4) => {
            conn.query('select rm.id idrm, rm.building_id idbu ,bu.name namebu, number, rm.peoplenumber nump from building bu left join room rm on rm.building_id = bu.id where bu.id = 12 ;', [id], (err7, bu5) => {
              conn.query('select rm.id idrm, rm.building_id idbu ,bu.name namebu, number, rm.peoplenumber nump from building bu left join room rm on rm.building_id = bu.id where bu.id = 13 ;', [id], (err8, bu6) => {
                conn.query('select rm.id idrm, rm.building_id idbu ,bu.name namebu, number, rm.peoplenumber nump from building bu left join room rm on rm.building_id = bu.id where bu.id = 14 ;', [id], (err9, bu7) => {
                  res.render('tanant/tanantAddEditBuilding', {
                    session: req.session,
                    data1: bu1,
                    data2: bu2,
                    data3: bu3,
                    data4: bu4,
                    data5: bu5,
                    data6: bu6,
                    data7: bu7,
                    data: id,
                    datast: idst,
                    datastrm: strm_id,
                    da: numrm
                  });
                });
              });
            });
          });
        });
      });
    });
  });
};
module.exports = controller;
