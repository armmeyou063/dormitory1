const controller ={};
const { validationResult } = require('express-validator');

controller.list = (req,res) => {
    req.getConnection((err,conn) =>{
        conn.query('select s.id as sid ,s.firstname as fname ,s.lastname as lname , s.nickname as nname , s.numberid as snum ,br.name as brname ,s.phone as sphone,DATE_FORMAT(birthday, "วันที่ %d/%m/%Y")birthday, bu.name as buname ,r.number as number,s.nfather as nfather ,s.lfather as lfather,s.nmother as nmother , s.lmother as lmother,sta.name as staname , sup.name as supname,s.careerfather as careerfather,s.careermother as careermother,s.brother as brother,s.studybro as studybro,s.careerbro as careerbro from student as s left join branch as br on s.branch_id = br.id left join bursary as bur on s.bursary_id = bur.id left join tribe as tr on s.tribe_id = tr.id left join support as sup on s.support_id = sup.id left join status as sta on s.status_id = sta.id left join room as r on s.room_id = r.id left join building as bu on s.building_id = bu.id;',(err,familytype) =>{
            if(err){
                res.json(err);
            }
            res.render('family/familyList',{
              session: req.session,
              data:familytype,
              });
            });
        });
};

controller.report=(req,res)=>{
const { id }=req.params;
req.getConnection((err,conn)=>{
  conn.query('select s.id as sid ,s.firstname as fname ,s.lastname as lname , s.nickname as nname , s.numberid as snum ,br.name as brname ,s.phone as sphone,DATE_FORMAT(birthday, "วันที่ %d/%m/%Y")birthday, bu.name as buname ,r.number as number,s.nfather as nfather ,s.lfather as lfather,s.nmother as nmother , s.lmother as lmother,sta.name as staname , sup.name as supname,s.careerfather as careerfather,s.careermother as careermother,s.brother as brother,s.studybro as studybro,s.careerbro as careerbro from student as s left join branch as br on s.branch_id = br.id left join bursary as bur on s.bursary_id = bur.id left join tribe as tr on s.tribe_id = tr.id left join support as sup on s.support_id = sup.id left join status as sta on s.status_id = sta.id left join room as r on s.room_id = r.id left join building as bu on s.building_id = bu.id;',[id],(err,familyreport) =>{
    if (err) {
      res.json(err);
    }
    res.render('family/familyReport',{
      session: req.session,
      data:familyreport[0]
    });
  });
});
};

module.exports = controller;
