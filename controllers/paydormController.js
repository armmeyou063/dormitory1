const controller = {};
const { validationResult } = require('express-validator');
const { default: validator } = require('validator');

controller.paydorm_bill = (req, res) => {
    var year = new Date().getFullYear() + 543
    req.getConnection((err, conn) =>
        conn.query('select * from bill where year = ?', [year], ((rr, bill) => {
            res.render('paydorm/paydorm_bill', {
                session: req.session, bill
            })
        })
        ))
}


controller.paydorm_build = (req, res) => {
    const { billid } = req.params;
    req.getConnection((err, conn) => {
        conn.query('select * from building', (err, building) => {
            res.render('paydorm/paydorm_build', {
                session: req.session,
                building, billid
            });
        });
    })
};

controller.paydorm_room = (req, res) => {
    const { buid } = req.params;
    const { billid } = req.params;
    req.getConnection((err, conn) => {
        conn.query('select * from building where id = ?', [buid], (err, building) => {
            conn.query('select room.*, price ,typebu_id, payroom.id as payid   from room  join building on room.building_id = building.id  join paybuild on paybuild.building_id = building.id left join payroom on payroom.room_id = room.id where building.id = ?;', [buid], (err, room) => {
                res.render('paydorm/paydorm_room', {
                    session: req.session,
                    buid, room, building, billid
                });
            });
        });
    })
};

controller.paydorm_student = (req, res) => {
    const { rmid } = req.params;
    const { stid } = req.params;
    const { billid } = req.params;
    req.getConnection((err, conn) => {
        conn.query('select  student.* , price, typebu_id,room.id as rmid , payroom.id as payid from student join current on current.student_id = student.id join room on current.room_id = room.id join building on current.building_id = building.id join paybuild on paybuild.building_id = building.id  left join payroom on payroom.student_id = student.id where room.id = ?;', [rmid], (err, student) => {
            conn.query('select count(student_id) as study from current join room on current.room_id = room.id  where room.id = ?;', [rmid], (err, study) => {
                conn.query('select * from room where id = ?', [rmid], (err, room) => {
                    conn.query('select * from student where id = ?', [stid], (err, st) => {
                        res.render('paydorm/paydorm_student', {
                            session: req.session,
                            rmid, student, stid, st, room, billid,study
                        });
                    });
                });
            });
        });
    })
};

controller.paydorm_add = (req, res) => {
    const { rmid } = req.params;
    const { stid } = req.params;
    const { billid } = req.params;
    req.getConnection((err, conn) => {
        if (stid) {
            conn.query('select st.* , cu.id as cuid, price, typebu_id from current as cu join student as st on cu.student_id = st.id join paybuild on paybuild.building_id = cu.building_id where st.id = ?;', [stid], (err, student) => {
                res.render('paydorm/paydorm_add', {
                    session: req.session, student, rmid, stid, billid
                });
            })
        } else {
            console.log("else");
            conn.query(' select rm.*,  price ,typebu_id from  room as rm  left join paybuild on paybuild.building_id = rm.building_id  where rm.id = ?;', [rmid], (err, student) => {
                console.log(student);
                console.log(rmid);
                res.render('paydorm/paydorm_add', {
                    session: req.session, student, rmid, stid, billid
                });
            })
        }
    });
};

controller.paydorm_save = (req, res) => {
    const data = req.body;
    const { rmid } = req.params;
    const { stid } = req.params;
    const { billid } = req.params;
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        req.session.errors = errors;
        req.session.success = false;
        res.redirect('/home');
    } else {
        req.session.success = true;
        req.session.topic = "เพิ่มข้อมูลเสร็จแล้ว";
        req.getConnection((err, conn) => {
            conn.query('INSERT INTO  pay  (room_id ,student_id , number , date ,payment,bill_id )  values(?,?,?,?,?,?)', [rmid, stid, data.number, data.date, data.payment, billid], (err, paydormsave) => {
                console.log(err);
                res.redirect('/');
            });
        });
    };
};

// controller.repair_confirm = (req, res) => {
//   req.getConnection((err, conn) => {
//     conn.query('select re.* from repair as re  join  building as bu on re.building_id = bu.id join room as rm on re.room_id = rm.id where re.confirm = 0 ', (err, repair) => {
//       res.render('repair/repair_confirm', {
//         session: req.session,
//         repair
//       });
//     });
//   });
// };

// controller.repair_saveconfirm = (req, res) => {
//   const { id } = req.params;
//   req.getConnection((err, conn) => {
//     conn.query('UPDATE repair set confirm = 1  where id = ? ', [id], (err, repair) => {
//       if (err) {
//         res.json(err);
//       }
//       res.redirect('/repair/repair_date');
//     });
//   });
// };


// controller.repair_date = (req, res) => {
//   req.getConnection((err, conn) => {
//     conn.query('select re.* from repair as re  join  building as bu on re.building_id = bu.id join room as rm on re.room_id = rm.id where re.confirm = 1 ', (err, repair) => {
//       res.render('repair/repair_date', {
//         session: req.session,
//         repair
//       });
//     });
//   });
// };

// controller.repair_savedate = (req, res) => {
//   const { id } = req.params;
//   req.getConnection((err, conn) => {
//     conn.query('UPDATE repair set  datetime = now()  where id = ? ', [id], (err, repair) => {
//       if (err) {
//         res.json(err);
//       }
//       res.redirect('/home');
//     });
//   });
// };
module.exports = controller;
