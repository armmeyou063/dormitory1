const controller = {};
const { validationResult } = require('express-validator');


//-------------------------------------------------------------------------------------------------------
//ส่วนข้อมูลหอพัก
//-------------------------------------------------------------------------------------------------------
controller.unidorm = function (req, res) {
  if (req.session.user) {
    req.getConnection((err, conn) => {
      conn.query('select * from building', (err, building) => {
        res.render('student/studentdorm', {
          building, session: req.session
        });
      });
    });
  } else {
    res.redirect('/');
  };
};

//-------------------------------------------------------------------------------------------------------
//ส่วนแสดงข้อมูลรายชื่อนักเรียน
//-------------------------------------------------------------------------------------------------------
controller.studentlist = (req, res) => {
  const { buid } = req.params;
  req.getConnection((err, conn) => {
    conn.query(' select st.*, fa.name as faname, rm.number as roomnumber,DATE_FORMAT(DATE_ADD(birthday, INTERVAL 543 YEAR), " %d/%m/%Y") as stbirthday from current as cu join student as st on cu.student_id = st.id join faculty as fa on st.faculty_id = fa.id join room as rm on cu.room_id = rm.id where cu.building_id = ?', [buid], (err, student) => {
      conn.query('select * from building where id = ?;', [buid], (err, bu) => {
        res.render('student/studentList', {
          session: req.session,
          student,
          bu,
          buid
        });
      });
    });
  });
};


//-------------------------------------------------------------------------------------------------------
//ส่วนกรอกข้อมูลในการย้ายหอพัก เข้าโดยการค้นหา
//-------------------------------------------------------------------------------------------------------
controller.addmoveout = (req, res) => {
  const { buid } = req.params;
  const { stid } = req.params;
  if (stid) {
    req.getConnection((err, conn) => {
      conn.query('select st.*,fa.name as faname, br.name as brname, bu.name as buname, rm.number as roomnumber from student as st join faculty as fa on st.faculty_id = fa.id join bursary as br on st.bursary_id = br.id join current as cu on cu.student_id = st.id join building as bu on cu.building_id = bu.id join room as rm on cu.room_id = rm.id  where st.id = ?;', [stid], (err, student) => {
        conn.query('select * from moveout where student_id = ?', [stid], (err, moveout) => {
          conn.query('select * from building;', (err, building) => {
            conn.query('select * from room;', (err, room) => {
              res.render('tanant_in_out/tanantoutAdd', {
                student,
                buid,
                stid,
                building,
                room,
                moveout,
                session: req.session
              });
            });
          });
        });
      });
    });
  } else {
    res.render('tanant_in_out/tanantout',{
      session: req.session
    });
  };
};

//-------------------------------------------------------------------------------------------------------
//ส่วนกรอกข้อมูลในการย้ายหอพัก เข้าโดยผ่าน admin
//-------------------------------------------------------------------------------------------------------
controller.moveoutadd = (req, res) => {
  var data = req.body;
  var buid = null;
  var search = data.numberid[0] + data.numberid[1] + data.numberid[2] + data.numberid[3] + data.numberid[4] + data.numberid[5] + data.numberid[6] + data.numberid[7] + data.numberid[8] + data.numberid[9] + data.numberid[10] + data.numberid[11] + data.numberid[12];
  req.getConnection((err, conn) => {
    conn.query('select st.*,fa.name as faname, br.name as brname, bu.name as buname, rm.number as roomnumber from student as st join faculty as fa on st.faculty_id = fa.id join bursary as br on st.bursary_id = br.id join current as cu on cu.student_id = st.id join building as bu on cu.building_id = bu.id join room as rm on cu.room_id = rm.id  where st.number = ?;', [search], (err, student) => {
      if (student.length > 0) {
        conn.query('select * from building;', (err, building) => {
          conn.query('select * from room;', (err, room) => {
            res.render('tanant_in_out/tanantoutAdd', {
              student,
              building,
              room,
              buid,
              session: req.session
            });
          });
        });
      } else {
        res.redirect('/studentmoveout')
      }
    });
  });
};

//-------------------------------------------------------------------------------------------------------
//ส่วนบันทึกข้อมูลในคำร้องขอย้ายออก
//-------------------------------------------------------------------------------------------------------
controller.savemoveout = (req, res) => {
  const data = req.body;
  const errors = validationResult(req);
  const { stid } = req.params;
  if (!errors.isEmpty()) {
    req.session.errors = errors;
    req.session.success = false;
    res.redirect('/home');
  } else {
    req.session.success = true;
    req.session.topic = "เพิ่มข้อมูลเสร็จแล้ว";
    req.getConnection((err, conn) => {
      conn.query('INSERT INTO moveout  (student_id  , because , object,place)  values(?,?,?,?)', [stid, data.because, data.object,data.place], (err, saveout) => {
        console.log(err);
        if (stid) {
          res.redirect('/tanantout/moveout');
        } else {
          res.redirect('/');
        }
      });
    });
  };
};

//-------------------------------------------------------------------------------------------------------
//ส่วนกรอกข้อมูลในการย้ายหอพัก เข้าโดยการค้นหา
//-------------------------------------------------------------------------------------------------------
controller.addmovein = (req, res) => {
  const { buid } = req.params;
  const { stid } = req.params;
  req.getConnection((err, conn) => {
    if (stid) {
      conn.query('select st.*,fa.name as faname, br.name as brname, bu.name as buname, rm.number as roomnumber from student as st join faculty as fa on st.faculty_id = fa.id join bursary as br on st.bursary_id = br.id join current as cu on cu.student_id = st.id join building as bu on cu.building_id = bu.id join room as rm on cu.room_id = rm.id  where st.id = ?;', [stid], (err, student) => {
        conn.query('select * from building;', (err, building) => {
          conn.query('select * from movein where student_id = ? ', [stid], (err, movein) => {
            conn.query('select * from room;', (err, room) => {
              res.render('tanant_in_out/tanantinAdd', {
                student: student,
                buid: buid,
                stid: stid,
                building: building,
                room: room,
                movein,
                session: req.session
              });
            });
          });
        });
      });
    } else {
      res.render('tanant_in_out/tanantin', {
        session: req.session
      });
    }
  });
};

//-------------------------------------------------------------------------------------------------------
//ส่วนกรอกข้อมูลในการย้ายหอพัก เข้าโดยการผ่าน admin
//-------------------------------------------------------------------------------------------------------
controller.moveinadd = (req, res) => {
  var data = req.body;
  var buid = null;
  var search = data.numberid[0] + data.numberid[1] + data.numberid[2] + data.numberid[3] + data.numberid[4] + data.numberid[5] + data.numberid[6] + data.numberid[7] + data.numberid[8] + data.numberid[9] + data.numberid[10] + data.numberid[11] + data.numberid[12];
  req.getConnection((err, conn) => {
    conn.query('select st.*,fa.name as faname, br.name as brname, bu.name as buname, rm.number as roomnumber from student as st join faculty as fa on st.faculty_id = fa.id join bursary as br on st.bursary_id = br.id join tanant as tn on tn.student_id = st.id join building as bu on tn.building_id = bu.id join room as rm on tn.room_id = rm.id  where st.numberid = ?;', [search], (err, student) => {
      if (student.length > 0) {
        conn.query('select * from movein where student_id = ?', [student[0].id], (err, movein) => {
          conn.query('select * from building;', (err, building) => {
            if (movein.length > 0) {
              var sql = 'select * from room where building_id = '+movein[0].building_id+';'
            }else {
              var sql = 'select * from room where building_id = 0;'
            }
            conn.query(sql, (err, room) => {
              res.render('tanant_in_out/tanantinAdd', {
                student,
                movein,
                building,
                room,
                buid,
                session: req.session
              });
            });
          });
        });
      } else {
        res.redirect('/studentmovein')
      }
    });
  });
};


//-------------------------------------------------------------------------------------------------------
//ส่วนบันทึกข้อมูลในคำร้องขอย้ายหอพัก
//-------------------------------------------------------------------------------------------------------
controller.savemovein = (req, res) => {
  const data = req.body;
  const errors = validationResult(req);
  const { stid } = req.params;
  if (!errors.isEmpty()) {
    req.session.errors = errors;
    req.session.success = false;
    res.redirect('/home');
  } else {
    req.session.success = true;
    req.session.topic = "เพิ่มข้อมูลเสร็จแล้ว";
    req.getConnection((err, conn) => {
      conn.query('INSERT INTO movein (student_id  , because , object, building_id , room_id )  values(?,?,?,?,?)', [stid, data.because, data.object, data.building_id, data.room_id], (err, saveout) => {
        console.log(err);
        if (stid) {
          res.redirect('/tanantout/movein');
        } else {
          res.redirect('/');
        }
      });
    });
  };
};

//-------------------------------------------------------------------------------------------------------
//ส่วนยกเลิกคำร้องย้ายหอ
//-------------------------------------------------------------------------------------------------------
controller.deletemovein = (req, res) => {
  const { id } = req.params;
  console.log(id);
  req.getConnection((err, conn) => {
    conn.query('DELETE from movein where id = ?;', [id], (err, moveout) => {
      console.log(err);
      if (err) {
        const errorss = { errors: [{ value: '', msg: 'การลบไม่ถูกต้อง', param: '', location: '' }] }
        req.session.errors = errorss;
        req.session.success = false;
      } else {
        req.session.success = true;
        req.session.topic = "ลบข้อมูลเสร็จแล้ว";
      };
      res.redirect('/');
    });
  });
};





controller.save = (req, res) => {
  const { id } = req.params;
  const data = req.body;
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    req.session.errors = errors;
    req.session.success = false;
    req.getConnection((err, conn) => {
      conn.query('SELECT * FROM branch', (err, branchtype) => {
        conn.query('select s.firstname as fname ,s.lastname as lname , s.nickname as nname , s.numberid as snum ,br.name as brname ,s.phone as sphone,DATE_FORMAT(birthday, "วันที่ %d/%m/%Y")birthday,DATE_FORMAT(s.datein, "วันที่ %d/%m/%Y")dateins from student as s left join branch as br on s.branch_id = br.id left join bursary as bur on s.bursary_id = bur.id left join tribe as tr on s.tribe_id = tr.id left join support as sup on s.support_id = sup.id left join status as sta on s.status_id = sta.id left join room as r on s.room_id = r.id left join building as bu on s.building_id = bu.id left join faculty as f on br.faculty_id = f.id left join classn as c on s.classn_id = c.id left join tanant  as t on t.room_id = t.id ;', (err, studenttype) => {
          conn.query('SELECT * FROM bursary', (err2, bursarytype) => {
            conn.query('SELECT * FROM tribe', (err3, tribetype) => {
              conn.query('SELECT * FROM student', (err4, studenttype) => {
                conn.query('SELECT * FROM status', (err5, statustype) => {
                  conn.query('SELECT * FROM support', (err6, supporttype) => {
                    conn.query('SELECT * FROM room', (err7, roomtype) => {
                      conn.query('SELECT * FROM building', (err8, buildingtype) => {
                        conn.query('SELECT * FROM faculty', (err9, facultype) => {
                          conn.query('SELECT * FROM classn', (err10, classtype) => {
                            conn.query('SELECT * FROM tanant', (err10, tananttype) => {
                              res.render('student/studentAdd', {
                                data1: branchtype,
                                data: studenttype,
                                data2: bursarytype,
                                data3: tribetype,
                                data4: statustype,
                                data5: supporttype,
                                data6: roomtype,
                                data7: buildingtype,
                                data8: facultype,
                                data9: classtype,
                                data10: tananttype,
                                idrm: idrm,
                                idbu: idbu,
                                session: req.session
                              });
                            });
                          });
                        });
                      });
                    });
                  });
                });
              });
            });
          });
        });
      });
    });
  } else {
    req.session.success = true;
    req.session.topic = "เพิ่มข้อมูลเสร็จแล้ว";
    req.getConnection((err, conn) => {
      if (id) {
        console.log('have id');
        conn.query('UPDATE student set ? where id = ?;', [data, id], (err, studenttype1) => {
          conn.query('INSERT INTO tanant set student_id = ?, room_id = ?;', [id, data.room_id], (err, studenttype2) => {
            conn.query('DELETE FROM tanantout WHERE student_id= ?', [id], (err, roomtype) => {
              res.redirect('/tanant');
            });;
          });
        });
      } else {
        conn.query('INSERT INTO student set ? ', [data], (err, studenttype1) => {
          conn.query('INSERT INTO tanant set student_id = ?, room_id = ?', [id, data.room_id], (err, studenttype2) => {
            conn.query('DELETE FROM tanantout WHERE student_id= ?', [id], (err, roomtype) => {
              res.redirect('/tanant');
            });
          });
        });
      }

    });

  };
};

controller.del = (req, res) => {
  const {
    id
  } = req.params;
  req.getConnection((err, conn) => {
    conn.query('select s.id as sid ,s.firstname as fname ,s.lastname as lname , s.nickname as nname , s.numberid as snum ,br.name as brname ,bur.name as burname,tr.name as trname,s.studentid as stid, s.phone as sphone,DATE_FORMAT(birthday, "วันที่ %d/%m/%Y")birthday,DATE_FORMAT(datein, "วันที่ %d/%m/%Y")datein, s.age as age ,s.bloodtype as bloodtype,s.talent as talent, s.phone as phone ,s.numberhome as numberhome,s.moo as moo,s.tombon as tombon ,s.aumper as aumper ,s.province as province ,s.nfather as nfather ,s.lfather as lfather, s.postalcode as postalcode,bu.name as buname, r.number as number,f.name as facname,c.nclass as nclass,s.nmother as nmother,s.lmother as lmother,sta.name as staname, s.careerfather as careerfather,s.careermother as careermother,s.brother as brother,s.studybro as studybro,s.careerbro as careerbro,sup.name as supname from student as s left join branch as br on s.branch_id = br.id left join bursary as bur on s.bursary_id = bur.id left join tribe as tr on s.tribe_id = tr.id left join support as sup on s.support_id = sup.id left join status as sta on s.status_id = sta.id left join room as r on s.room_id = r.id left join building as bu on s.building_id = bu.id left join faculty as f on br.faculty_id = f.id left join classn as c on s.classn_id = c.id left join tanant  as t on t.room_id = t.id where s.id = ?', [id], (err, student) => {

      if (err) {
        res.json(err);
      }
      res.render('student/studentDelete', {
        session: req.session,
        data: student[0]

      });
    });
  });
};
controller.delete = (req, res) => {
  const {
    id
  } = req.params;
  req.getConnection((err, conn) => {
    conn.query('DELETE FROM student WHERE id= ?', [id], (err, studenttype) => {
      conn.query('DELETE FROM tanant WHERE student_id= ?', [id], (err, studenttype) => {
        if (err) {
          const errorss = {
            errors: [{
              value: '',
              msg: 'การลบไม่ถูกต้อง',
              param: '',
              location: ''
            }]
          }
          req.session.errors = errorss;
          req.session.success = false;
        } else {
          req.session.success = true;
          req.session.topic = "ลบข้อมูลเสร็จแล้ว";
        }
        console.log(studenttype);
        res.redirect('/student');
      });
    });
  });
};
controller.edit = (req, res) => {
  const {
    id
  } = req.params;
  req.getConnection((err, conn) => {
    conn.query('select * from student where id = ?', [id], (err, studentype) => {
      conn.query('select DATE_FORMAT(datein, "%Y-%m-%d")datein1,DATE_FORMAT(birthday, "%Y-%m-%d")birthday1  from student where id = ?;', [id], (err3, studentype1) => {
        conn.query('SELECT * FROM branch', (err1, branchtype) => {
          conn.query('SELECT * FROM building', (err3, buildingtype) => {
            conn.query('SELECT * FROM room', (err2, roomtype) => {
              conn.query('SELECT * FROM status', (err4, statustype) => {
                conn.query('SELECT * FROM support', (err5, supporttype) => {
                  conn.query('SELECT * FROM bursary', (err6, bursarytype) => {
                    conn.query('SELECT * FROM tribe', (err7, tribetype) => {
                      conn.query('SELECT * FROM faculty', (err8, facultype) => {
                        conn.query('SELECT * FROM classn', (err9, classtype) => {
                          conn.query('SELECT * FROM tanant', (err9, tananttype) => {
                            res.render('student/studentUpdate', {
                              session: req.session,
                              data: studentype[0],
                              data1: branchtype,
                              data2: roomtype,
                              data3: buildingtype,
                              data4: studentype1[0],
                              data5: statustype,
                              data8: supporttype,
                              data6: bursarytype,
                              data7: tribetype,
                              data10: facultype,
                              data9: classtype,
                              data11: tananttype
                            });
                          });
                        });
                      });
                    });
                  });
                });
              });
            });
          });
        });
      });
    });
  });
};

controller.update = (req, res) => {
  const errors = validationResult(req);
  const {
    id
  } = req.params;
  const data = req.body;
  if (!errors.isEmpty()) {
    req.session.errors = errors;
    req.session.success = false;
    req.getConnection((err, conn) => {
      conn.query('select * from student where id = ?', [id], (err, studentype) => {
        conn.query('select DATE_FORMAT(datein, "%Y-%m-%d")datein1,DATE_FORMAT(birthday, "%Y-%m-%d")birthday1  from student where id = ?;', [id], (err3, studentype1) => {
          conn.query('SELECT * FROM branch', (err1, branchtype) => {
            conn.query('SELECT * FROM building', (err3, buildingtype) => {
              conn.query('SELECT * FROM room', (err2, roomtype) => {
                conn.query('SELECT * FROM status', (err4, statustype) => {
                  conn.query('SELECT * FROM support', (err5, supporttype) => {
                    conn.query('SELECT * FROM bursary', (err6, bursarytype) => {
                      conn.query('SELECT * FROM tribe', (err7, tribetype) => {
                        conn.query('SELECT * FROM faculty', (err8, facultype) => {
                          conn.query('SELECT * FROM classn', (err9, classtype) => {
                            conn.query('SELECT * FROM tanant', (err9, tananttype) => {
                              res.render('student/studentUpdate', {
                                session: req.session,
                                data: studentype[0],
                                data1: branchtype,
                                data2: roomtype,
                                data3: buildingtype,
                                data4: studentype1[0],
                                data5: statustype,
                                data8: supporttype,
                                data6: bursarytype,
                                data7: tribetype,
                                data10: facultype,
                                data9: classtype,
                                data11: tananttype
                              });
                            });
                          });
                        });
                      });
                    });
                  });
                });
              });
            });
          });
        });
      });
    });
  } else {

    req.session.success = true;
    req.session.topic = "แก้ไขข้อมูลเสร็จแล้ว";
    req.getConnection((err, conn) => {
      conn.query('UPDATE student SET ?  WHERE id = ?', [data, id], (err, student) => {

        if (err) {
          console.log(err);
        }
        res.redirect('/student');
      });
    });
  };

};

controller.add = (req, res) => {
  const {
    idrm
  } = req.params;
  const {
    idbu
  } = req.params;
  const {
    tanantroomid
  } = req.params;

  req.getConnection((err, conn) => {
    conn.query('SELECT * FROM tanantout where student_id=?', [tanantroomid], (err10, tanantouttype) => {

      conn.query('SELECT * FROM branch', (err, branchtype) => {
        conn.query('select * from student where id = ?', [tanantroomid], (err, studenttype1) => {

          conn.query('select c.id as cid,tr.id as trid,bur.id as burid,sta.id as staid,sup.id as supid,br.id as brid,s.firstname as fname ,s.lastname as lname , s.nickname as nname , s.numberid as snum ,br.name as brname ,s.phone as sphone,DATE_FORMAT(birthday, "วันที่ %d/%m/%Y")birthday,DATE_FORMAT(s.datein, "วันที่ %d/%m/%Y")dateins from student as s left join branch as br on s.branch_id = br.id left join bursary as bur on s.bursary_id = bur.id left join tribe as tr on s.tribe_id = tr.id left join support as sup on s.support_id = sup.id left join status as sta on s.status_id = sta.id left join room as r on s.room_id = r.id left join building as bu on s.building_id = bu.id left join faculty as f on br.faculty_id = f.id left join classn as c on s.classn_id = c.id left join tanant  as t on t.room_id = t.id ;', (err, studenttype) => {
            conn.query('SELECT * FROM bursary', (err2, bursarytype) => {
              conn.query('SELECT * FROM tribe', (err3, tribetype) => {
                conn.query('SELECT * FROM status', (err5, statustype) => {
                  conn.query('SELECT * FROM support', (err6, supporttype) => {
                    conn.query('SELECT * FROM room', (err7, roomtype) => {
                      conn.query('SELECT * FROM building', (err8, buildingtype) => {
                        conn.query('SELECT * FROM faculty', (err9, facultype) => {
                          conn.query('SELECT * FROM classn', (err10, classtype) => {
                            conn.query('SELECT * FROM tanant', (err10, tananttype) => {
                              console.log(studenttype);

                              res.render('student/studentAdd', {
                                data1: branchtype,
                                data99: studenttype[0],
                                data: studenttype1[0],
                                data2: bursarytype,
                                data3: tribetype,
                                data4: statustype,
                                data5: supporttype,
                                data6: roomtype,
                                data7: buildingtype,
                                data8: facultype,
                                data9: classtype,
                                data10: tananttype,
                                idrm: idrm,
                                idbu: idbu,
                                tanantroomid: tanantroomid,
                                session: req.session
                              });

                            });
                          });
                        });
                      });
                    });
                  });
                });
              });
            });
          });
        });
      });


    });
  });
};

controller.editRoom = (req, res) => {
  const {
    idrm
  } = req.params;
  const {
    idbu
  } = req.params;
  const {
    idnump
  } = req.params;
  const {
    idst
  } = req.params;
  const {
    strm_id
  } = req.params;
  const {
    numrm
  } = req.params;

  req.getConnection((err, conn) => {
    conn.query('UPDATE student set room_id = ?, building_id = ? where id = ?', [idrm, idbu, idst], (err10, tanantouttype1) => {
      conn.query('UPDATE tanant set room_id = ? where student_id = ?', [idrm, idst], (err11, tanantouttype5) => {
        res.redirect('/tanant');
      });
    });
  });
};

controller.report = (req, res) => {
  const {
    id
  } = req.params;
  req.getConnection((err, conn) => {
    conn.query('select s.id as sid ,s.firstname as fname ,s.lastname as lname , s.nickname as nname , s.numberid as snum ,br.name as brname ,bur.name as burname,f.name as facname,tr.name as trname,s.studentid as stid, s.phone as sphone,DATE_FORMAT(birthday, "วันที่ %d/%m/%Y")birthday,DATE_FORMAT(datein, "วันที่ %d/%m/%Y")datein, s.age as age ,s.bloodtype as bloodtype,s.talent as talent, s.phone as phone ,s.numberhome as numberhome,s.moo as moo,s.tombon as tombon ,s.aumper as aumper ,s.province as province ,s.nfather as nfather ,s.lfather as lfather, s.postalcode as postalcode,bu.name as buname, r.number as number,f.name as facname,c.nclass as nclass,s.nmother as nmother,s.lmother as lmother,sta.name as staname, s.careerfather as careerfather,s.careermother as careermother,s.brother as brother,s.studybro as studybro,s.careerbro as careerbro,sup.name as supname from student as s left join branch as br on s.branch_id = br.id left join bursary as bur on s.bursary_id = bur.id left join tribe as tr on s.tribe_id = tr.id left join support as sup on s.support_id = sup.id left join status as sta on s.status_id = sta.id left join room as r on s.room_id = r.id left join building as bu on s.building_id = bu.id left join faculty as f on s.faculty_id = f.id left join classn as c on s.classn_id = c.id left join tanant  as t on t.room_id = t.id where s.id = ?', [id], (err, tribemodeList) => {
      if (err) {
        res.json(err);
      }
      res.render('student/studentReport', {
        session: req.session,
        data: tribemodeList[0]
      });
    });
  });
};

// controller for ajax

controller.getroom = (req,res) =>{
  var url = req.params;
  req.getConnection((err,conn)=>{
    conn.query('select * from room where building_id = ?',[url.buid],(err,room)=>{
      res.send(room);
    });
  });
}

module.exports = controller;
