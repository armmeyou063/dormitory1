const controller = {};
const { validationResult } = require('express-validator');

controller.historylistout = (req, res) => {
    req.getConnection((err, conn) => {
      conn.query('select mo.id as moid, st.*, fa.name as faname, bu.name as buname, rm.number as roomnumber from moveout as mo join student as st on mo.student_id = st.id join faculty as fa on st.faculty_id = fa.id  join tanant as tn on tn.student_id = st.id join building as bu on tn.building_id = bu.id join room as rm on tn.room_id = rm.id where mo.confirm = 1 ;', (err, historyout) => {
        if (err) {
          res.json(err);
        }
        res.render('history/history_moveout', {
          session: req.session,
          historyout: historyout
        });
      });
    });
  };


module.exports = controller;
