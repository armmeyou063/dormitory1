const controller ={};
const { validationResult } = require('express-validator');

controller.list = (req,res) => {
    req.getConnection((err,conn) =>{
        conn.query('select * from building;',(err,buildingtype) =>{
            if(err){
                res.json(err);
            }
            res.render('building/buildingList',{
              session: req.session,
              data:buildingtype,
              });
            });
        });
};
controller.save = (req,res) => {
    const data=req.body;
       const errors = validationResult(req);
        if(!errors.isEmpty()){
            req.session.errors=errors;
            req.session.success=false;
            res.redirect('/building/add');
        }else{
            req.session.success=true;
            req.session.topic="เพิ่มข้อมูลเสร็จแล้ว";
            req.getConnection((err,conn)=>{
            conn.query('INSERT INTO building set ?',[data],(err,buildingtype)=>{
            res.redirect('/building');
            });
        });
    };
};
controller.del = (req,res) => {
    const { id } = req.params;
    req.getConnection((err,conn)=>{
        conn.query('select * from building HAVING id = ?',[id],(err,buildingtype)=>{
          if(err){
              res.json(err);
          }
          res.render('building/buildingDelete',{
            session: req.session,
            data:buildingtype[0]
          });
      });
  });
};
controller.delete = (req,res) => {
    const { id } = req.params;
    req.getConnection((err,conn)=>{
        conn.query('DELETE FROM building WHERE id= ?',[id],(err,buildingtype)=>{
            if(err){
              const  errorss= {errors: [{ value: '', msg: 'การลบไม่ถูกต้อง', param: '', location: '' }]}
              req.session.errors=errorss;
              req.session.success=false;
            }else {
                req.session.success=true;
                req.session.topic="ลบข้อมูลเสร็จแล้ว";
            }
            console.log(buildingtype);
            res.redirect('/building');
        });
    });
};
controller.edit = (req,res) => {
    const { id } = req.params;
        req.getConnection((err,conn)=>{
                conn.query('SELECT * FROM building WHERE id= ?',[id],(err,buildingtype)=>{
                        res.render('building/buildingUpdate',{
                          session: req.session,
                          data1:buildingtype[0]
                        });
                });
            });
}
controller.update = (req,res) => {
    const errors = validationResult(req);
    const { id } = req.params;
    const data = req.body;
        if(!errors.isEmpty()){
            req.session.errors=errors;
            req.session.success=false;
            req.getConnection((err,conn)=>{
                conn.query('SELECT * FROM building WHERE id= ?',[id],(err,buildingtype)=>{
                res.render('building/buildingUpdate',{
                  session: req.session,
                  data1:buildingtype
                });
              });
            });
        }else{
            req.session.success=true;
            req.session.topic="แก้ไขข้อมูลเสร็จแล้ว";
            req.getConnection((err,conn) => {
                conn.query('UPDATE  building SET ?  WHERE id = ?',[data,id],(err,buildingtype) => {
                  if(err){
                      res.json(err);
                  }
               res.redirect('/building');
               });
             });
        }
};
controller.add = (req,res) => {
    req.getConnection((err,conn) => {
      conn.query('SELECT * FROM building',(err,buildingtype) =>{
      res.render('building/buildingAdd',{
        data:buildingtype,
        session: req.session
      });
    });
  });
};
module.exports = controller;
