const controller ={};
const { validationResult } = require('express-validator');

controller.list = (req,res) => {
    req.getConnection((err,conn) =>{
        conn.query('select * from bursary;',(err,bursarytype) =>{
            if(err){
                res.json(err);
            }
            res.render('bursary/bursaryList',{session: req.session,
              data:bursarytype,
              });
            });
        });
};
controller.save = (req,res) => {
    const data=req.body;
       const errors = validationResult(req);
        if(!errors.isEmpty()){
            req.session.errors=errors;
            req.session.success=false;
            res.redirect('/bursary/add');
        }else{
            req.session.success=true;
            req.session.topic="เพิ่มข้อมูลเสร็จแล้ว";
            req.getConnection((err,conn)=>{
            conn.query('INSERT INTO bursary set ?',[data],(err,bursarytype)=>{
            res.redirect('/bursary');
            });
        });
    };
};
controller.del = (req,res) => {
    const { id } = req.params;
    req.getConnection((err,conn)=>{
        conn.query('select * from bursary HAVING id = ?',[id],(err,bursarytype)=>{
          if(err){
              res.json(err);
          }
          res.render('bursary/bursaryDelete',{
            session: req.session,
            data:bursarytype[0]
          });
      });
  });
};
controller.delete = (req,res) => {
    const { id } = req.params;
    req.getConnection((err,conn)=>{
        conn.query('DELETE FROM bursary WHERE id= ?',[id],(err,bursarytype)=>{
            if(err){
              const  errorss= {errors: [{ value: '', msg: 'การลบไม่ถูกต้อง', param: '', location: '' }]}
              req.session.errors=errorss;
              req.session.success=false;
            }else {
                req.session.success=true;
                req.session.topic="ลบข้อมูลเสร็จแล้ว";
            }
            console.log(bursarytype);
            res.redirect('/bursary');
        });
    });
};
controller.edit = (req,res) => {
    const { id } = req.params;
        req.getConnection((err,conn)=>{
                conn.query('SELECT * FROM bursary WHERE id= ?',[id],(err,bursarytype)=>{
                        res.render('bursary/bursaryUpdate',{
                          session: req.session,
                          data1:bursarytype[0],
                        });
                });
            });
}
controller.update = (req,res) => {
    const errors = validationResult(req);
    const { id } = req.params;
    const data = req.body;
        if(!errors.isEmpty()){
            req.session.errors=errors;
            req.session.success=false;
            req.getConnection((err,conn)=>{
                conn.query('SELECT * FROM bursary WHERE id= ?',[id],(err,bursarytype)=>{
                res.render('bursary/bursaryUpdate',{
                  session: req.session,
                  data1:bursarytype[0],
                });
              });
            });
        }else{
            req.session.success=true;
            req.session.topic="แก้ไขข้อมูลเสร็จแล้ว";
            req.getConnection((err,conn) => {
                conn.query('UPDATE  bursary SET ?  WHERE id = ?',[data,id],(err,bursarytype) => {
                  if(err){
                      res.json(err);
                  }
               res.redirect('/bursary');
               });
             });
        }
};
controller.add = (req,res) => {
    req.getConnection((err,conn) => {
      conn.query('SELECT * FROM bursary',(err,bursarytype) =>{
      res.render('bursary/bursaryAdd',{
        data:bursarytype,
        session: req.session
      });
    });
  });
};
module.exports = controller;
