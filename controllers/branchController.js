const controller ={};
const { validationResult } = require('express-validator');

controller.list = (req,res) => {
    req.getConnection((err,conn) =>{
        conn.query('select b.id as bid ,b.name as bname ,f.name as fname  from branch as b  join faculty as f on b.faculty_id = f.id ;',(err,branchtype) =>{
            if(err){
                res.json(err);
            }
            res.render('branch/branchList',{
              session: req.session,
              data:branchtype,
              });
            });
        });
};
controller.save = (req,res) => {
    const data=req.body;
       const errors = validationResult(req);
        if(!errors.isEmpty()){
            req.session.errors=errors;
            req.session.success=false;
            res.redirect('/branch/add');
        }else{
            req.session.success=true;
            req.session.topic="เพิ่มข้อมูลเสร็จแล้ว";
            req.getConnection((err,conn)=>{
            conn.query('INSERT INTO branch set ?',[data],(err,branchtype)=>{
            res.redirect('/branch');
            });
        });
    };
};
controller.del = (req,res) => {
    const { id } = req.params;
    req.getConnection((err,conn)=>{
        conn.query('select * from branch HAVING id = ?',[id],(err,branchtype)=>{
          conn.query('SELECT * FROM faculty',(err1,facultytype) =>{
          if(err){
              res.json(err);
          }else if (err1) {
              res.json(err1);
          }
          res.render('branch/branchDelete',{
            session: req.session,
            data:branchtype[0],
            data1:facultytype[0]
          });
        });
      });
  });
};
controller.delete = (req,res) => {
    const { id } = req.params;
    req.getConnection((err,conn)=>{
        conn.query('DELETE FROM branch WHERE id= ?',[id],(err,branchtype)=>{
            if(err){
              const  errorss= {errors: [{ value: '', msg: 'การลบไม่ถูกต้อง', param: '', location: '' }]}
              req.session.errors=errorss;
              req.session.success=false;
            }else {
                req.session.success=true;
                req.session.topic="ลบข้อมูลเสร็จแล้ว";
            }
            console.log(branchtype);
            res.redirect('/branch');
        });
    });
};
controller.edit = (req,res) => {
    const { id } = req.params;
        req.getConnection((err,conn)=>{
                conn.query('SELECT * FROM branch WHERE id= ?',[id],(err,branchtype)=>{
                  conn.query('SELECT * FROM faculty',(err,facultytype) =>{
                        res.render('branch/branchUpdate',{
                          session: req.session,
                          data1:branchtype[0],
                          data:facultytype
                        });
                      });
                });
            });
}
controller.update = (req,res) => {
    const errors = validationResult(req);
    const { id } = req.params;
    const data = req.body;
        if(!errors.isEmpty()){
            req.session.errors=errors;
            req.session.success=false;
            req.getConnection((err,conn)=>{
                conn.query('SELECT * FROM branch WHERE id= ?',[id],(err,branchtype)=>{
                  conn.query('SELECT * FROM faculty',(err,facultytype) =>{
                res.render('branch/branchUpdate',{
                  session: req.session,
                  data1:branchtype[0],
                  data:facultytype
                  });
                });
              });
            });
        }else{
            req.session.success=true;
            req.session.topic="แก้ไขข้อมูลเสร็จแล้ว";
            req.getConnection((err,conn) => {
                conn.query('UPDATE  branch SET ?  WHERE id = ?',[data,id],(err,branchtype) => {
                  if(err){
                      res.json(err);
                  }
               res.redirect('/branch');
               });
             });
        }
};
controller.add = (req,res) => {
    req.getConnection((err,conn) => {
        conn.query('SELECT * FROM faculty',(err,facultytype) =>{
          conn.query('select b.id as bid ,b.name as bname ,f.name as fname, f.id as fid  from branch as b  join faculty as f on b.faculty_id = f.id ;',(err,branchtype) =>{
            res.render('branch/branchAdd',{
          data1:facultytype,
          data:branchtype,
          session: req.session
        });
      });
    });
  });
};
module.exports = controller;
