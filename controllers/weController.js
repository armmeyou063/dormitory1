const controller = {};
const {validationResult} = require('express-validator');
const request = require('request');



controller.water_build = (req, res) => {
  req.getConnection((err, conn) => {
      conn.query('select * from building', (err, building) => {
          res.render('we/we_build', {
              session: req.session,
              building
          });
      });
  })
};


controller.list = (req, res) => {
  var url = req.params;
  var data = req.body;
  var d = new Date();
  var m = d.getMonth();
  if (m == 0) {
    m = 12;
  }
  if (m < 10) {
    m = '0'+m;
  }
  var y = d.getFullYear();
  var yth = d.getFullYear()+543;
  var mont = y+'-'+m;
  req.getConnection((err, conn) => {
    conn.query('select * from month', (err, month1) => {
      if (data) {
        for (var i = 0; i < month1.length; i++) {
          if (month1[i].name == data.month) {
            m = month1[i].id;
            if (m < 10) {
              m = '0'+m;
            }
            mont = (+(data.year-543))+'-'+m;
          }
        }
      }
      conn.query('select * from month where id = ?',[m],(err,month)=>{
        conn.query('select * from building where id = ?',[url.unid],(err,unidome)=>{
          conn.query('select we.* from we join room on room_id = room.id where building_id = ? and day = ?',[url.unid,mont],(err,we)=>{
            conn.query('select building.*,number,we.id as weid,newwater,oldwater,watercost,watermeter,newelec,oldelec,eleccost,elecmeter,concat(DATE_FORMAT(we.payday,"%d/%m/"),DATE_FORMAT(we.payday,"%Y")+543) as payday from building left join room on room.building_id = building.id left join (select * from we where day = ?) as we on we.room_id = room.id where building.id = ?;',[mont,url.unid], (err, building) => {
              if (err) {
                res.json(err);
              }
              res.render('we/weList', {
                month,yth,month1,url,unidome,we,building,session: req.session
              });
            });
          });
        });
      });
    });
  });
};

controller.unidome = function (req, res) {
  if (req.session.user) {
    var url = req.params;
    req.getConnection((err,conn)=>{
      conn.query('select * from building',(err,building)=>{
        res.render('unidome', {
          building,url,session: req.session
        });
      });
    });
  } else {
    res.redirect('/');
  }
};

controller.add = (req, res) => {
  var url = req.params;
  var dth = url.day.split('-')[1];
  var yth = url.day.split('-')[0];
  req.getConnection((err, conn) => {
    conn.query('select * from month', (err, month) => {
      var mon1,mon2;
      for (var i = 0; i < month.length; i++) {
        if (month[i].name == dth) {
          var m1 = month[i].id-1;
          if (m1 < 10) {
            m1 = "0"+m1;
          }
          mon1 = (+(url.day.split('-')[0])-543)+"-"+m1;
          var m2 = month[i].id;
          if (m2 < 10) {
            m2 = "0"+m2;
          }
          mon2 = (+(url.day.split('-')[0])-543)+"-"+m2;
        }
      }
      conn.query('select * from building where id = ?',[url.unid],(err,unidome)=>{
        conn.query('select   webuild.typewe_id as wetype , building.*,room.id as roid, number, we1.newwater as oldwater, we2.newwater as newwater, watermeter, watercost, we1.newelec as oldelec, we2.newelec as newelec, elecmeter, eleccost from building join room on room.building_id = building.id left join (select * from we where day = ?) as we1 on we1.room_id = room.id join webuild on webuild.building_id = building.id join typewe on webuild.typewe_id = typewe.id left join (select * from we where day = ?) as we2 on we2.room_id = room.id where building.id = ?;;',[mon1,mon2,url.unid], (err, building) => {
          if (err) {
            res.json(err);
          }
          res.render('we/weAdd', {
            month,building,mon2,dth,yth,unidome,session: req.session
          });
        });
      });
    });
  });
};

controller.addC = (req, res) => {
  req.getConnection((err, conn) => {
    conn.query('SELECT DATE_FORMAT(now(),"%Y-%m-%d") as datetime', (err, dateday) => {
      conn.query('SELECT DATE_FORMAT(? , "%d")d1,DATE_FORMAT(? , "%m")m2,DATE_FORMAT(DATE_ADD(?,INTERVAL 543 year) , "%Y")y2,DATE_FORMAT(DATE_SUB(?,INTERVAL 1 month) , "%m")m1,DATE_FORMAT(DATE_SUB(DATE_ADD(?,INTERVAL 543 year),INTERVAL 1 month) , "%Y")y1;', [dateday[0].datetime, dateday[0].datetime, dateday[0].datetime, dateday[0].datetime, dateday[0].datetime], (err, daytime) => {
        conn.query('select * from building;', (err, bu) => {
          if (daytime[0].d1 <= 5) {
            conn.query('select ew.day as day,rm.id as rmid,bu.id as idbu,bu.name as buname,rm.number as rmnumber, ew.newelec as newe, ew.newwater as neww from room as rm left join building as bu on rm.building_id = bu.id left join (select * from elecwater  WHERE day=?-? ) as ew on ew.room_id = rm.id ;', [daytime[0].y1, daytime[0].m1], (err, roomtype) => {
              conn.query('select * from month', (err, month) => {
                if (err) {
                  res.json(err);
                }
                res.render('we/weAdd', {
                  session: req.session,
                  data: roomtype,
                  data1: daytime[0],
                  month: month,
                  bu: bu
                });
              });
              // console.log('roomtype');
            });
          } else {
            conn.query('select ew.day as day,rm.id as rmid,bu.id as idbu,bu.name as buname,rm.number as rmnumber, ew.newelec as newe, ew.newwater as neww from room as rm left join building as bu on rm.building_id = bu.id left join (select * from elecwater  WHERE day=?-? ) as ew on ew.room_id = rm.id ;', [daytime[0].y2, daytime[0].m2], (err, roomtype) => {
              conn.query('select * from month', (err, month) => {
                if (err) {
                  res.json(err);
                }
                console.log('roomtype');
                res.render('we/weAdd', {
                  session: req.session,
                  data: roomtype,
                  data1: daytime[0],
                  month: month,
                  bu: bu
                });
              });
            });
          }
        });
      });
    });
  });
};

controller.save = (req, res) => {
  var url = req.params;
  const data = req.body;
  // console.log(data);
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    req.session.errors = errors;
    req.session.success = false;
    res.redirect('/we/add');
  } else {
    for (i = 0; i < data.newwater.length; i++) {
      req.getConnection((err, conn) => {
        conn.query('INSERT INTO we (room_id,newwater,oldwater,day,oldelec,newelec) values (?,?,?,?,?,?)', [data.room_id[i],data.newwater[i],data.oldwater[i],data.day[i],data.oldelec[i],data.newelec[i]], (err, statustype) => {
          if (err) {
            res.json(err);
          }else {
            req.session.success = true;
            req.session.topic = "เพิ่มข้อมูลเสร็จแล้ว";
          }
        });
      });
    };
    res.redirect('/we/list/'+url.unid);
  };
};

controller.edit = (req, res) => {
  var url = req.params;
  var dth = url.day.split('-')[1];
  var yth = url.day.split('-')[0];
  req.getConnection((err, conn) => {
    conn.query('select * from month', (err, month) => {
      var mon1,mon2;
      for (var i = 0; i < month.length; i++) {
        if (month[i].name == dth) {
          var m1 = month[i].id-1;
          if (m1 < 10) {
            m1 = "0"+m1;
          }
          mon1 = (+(url.day.split('-')[0])-543)+"-"+m1;
          var m2 = month[i].id;
          if (m2 < 10) {
            m2 = "0"+m2;
          }
          mon2 = (+(url.day.split('-')[0])-543)+"-"+m2;
        }
      }
      conn.query('select * from building where id = ?',[url.unid],(err,unidome)=>{
        conn.query('select building.*,room.id as roid, number, we2.id as weid, we1.newwater as oldwater, we2.newwater as newwater, watermeter, watercost, we1.newelec as oldelec, we2.newelec as newelec, elecmeter, eleccost from building join room on room.building_id = building.id left join (select * from we where day = ?) as we1 on we1.room_id = room.id left join (select * from we where day = ?) as we2 on we2.room_id = room.id where building.id = ?;',[mon1,mon2,url.unid], (err, building) => {
          if (err) {
            res.json(err);
          }
          res.render('we/weUpdate', {
            month,building,mon2,dth,yth,unidome,session: req.session
          });
        });
      });
    });
  });
}

controller.update = (req, res) => {
  const errors = validationResult(req);
  var url = req.params;
  const data = req.body;
  if (!errors.isEmpty()) {
    req.session.errors = errors;
    req.session.success = false;
    res.redirect('/we/edit/'+url.day+'/'+url.unid);
  } else {
    req.session.success = true;
    req.session.topic = "แก้ไขข้อมูลเสร็จแล้ว";
    req.getConnection((err, conn) => {
      for (var i = 0; i < data.id.length; i++) {
        conn.query('UPDATE we SET oldwater = ?, newwater = ?, oldelec = ?, newelec = ?  WHERE id = ?', [data.oldwater[i],data.newwater[i],data.oldelec[i],data.newelec[i],data.id[i]], (err,we) => {
          if (err) {
            res.json(err);
          }
        });
      }
      res.redirect('/we/list/'+url.unid);
    });
  }
};

controller.pay = (req, res) => {
  const errors = validationResult(req);
  var url = req.params;
  req.session.success = true;
  req.session.topic = "แก้ไขข้อมูลเสร็จแล้ว";
  req.getConnection((err, conn) => {
    conn.query('UPDATE we SET payday = now()  WHERE id = ?', [url.weid], (err, we) => {
      if (err) {
        res.json(err);
      }
      res.redirect('/we/list/'+url.unid);
    });
  });
};

controller.cpay = (req, res) => {
  const errors = validationResult(req);
  var url = req.params;
  req.session.success = true;
  req.session.topic = "แก้ไขข้อมูลเสร็จแล้ว";
  req.getConnection((err, conn) => {
    conn.query('UPDATE we SET payday = NULL  WHERE id = ?', [url.weid], (err, we) => {
      if (err) {
        res.json(err);
      }
      res.redirect('/we/list/'+url.unid);
    });
  });
};

controller.jsreport = (req, res) => {
  const {id} = req.params;
  req.getConnection((err, conn) => {
    conn.query('select (ew.newelec-ew.oldelec) as selec,ROUND((ew.newelec-ew.oldelec)*rm.eleccost,2) as unit,(ew.newwater-ew.oldwater) as selec1,ROUND((ew.newwater-ew.oldwater)*rm.watercost,2) as unit1,(ROUND((ew.newelec-ew.oldelec)*rm.eleccost,2)+rm.elecmeter) +(ROUND((ew.newwater-ew.oldwater)*rm.watercost,2)+rm.watermeter) as unit2 ,bu.name as buname, rm.number as number ,ew.day as day ,ew.oldelec as oelec,ew.newelec as nelec , rm.eleccost as ecost ,rm.elecmeter as emeter,ew.oldwater as owater,ew.newwater as nwater,rm.watercost as wcost,rm.watermeter as wmeter from elecwater as ew left join room as rm on ew.room_id = rm.id left join building as bu on rm.building_id = bu.id ;', [id], (err, ew) => {
      var data = {
        "template": {
          "shortid": "sl_gLr6U7H"
        },
        data: {
          ew
        }
      }
      var option = {
        url: 'http://127.0.0.1:5488/api/report',
        method: 'POST',
        json: data
      }
      request(option).pipe(res);
    });
  });
};

module.exports = controller;
