const controller ={};
const { validationResult } = require('express-validator');

controller.list = (req,res) => {
    req.getConnection((err,conn) =>{
        conn.query('select * from tribe;',(err,tribetype) =>{
            if(err){
                res.json(err);
            }
            res.render('tribe/tribeList',{
              session: req.session,
              data:tribetype,
              });
            });
        });
};
controller.save = (req,res) => {
    const data=req.body;
       const errors = validationResult(req);
        if(!errors.isEmpty()){
            req.session.errors=errors;
            req.session.success=false;
            res.redirect('/tribe/add');
        }else{
            req.session.success=true;
            req.session.topic="เพิ่มข้อมูลเสร็จแล้ว";
            req.getConnection((err,conn)=>{
            conn.query('INSERT INTO tribe set ?',[data],(err,tribetype)=>{
            res.redirect('/tribe');
            });
        });
    };
};
controller.del = (req,res) => {
    const { id } = req.params;
    req.getConnection((err,conn)=>{
        conn.query('select * from tribe HAVING id = ?',[id],(err,tribetype)=>{
          if(err){
              res.json(err);
          }
          res.render('tribe/tribeDelete',{
            session: req.session,
            data:tribetype[0]
          });
      });
  });
};
controller.delete = (req,res) => {
    const { id } = req.params;
    req.getConnection((err,conn)=>{
        conn.query('DELETE FROM tribe WHERE id= ?',[id],(err,tribetype)=>{
            if(err){
              const  errorss= {errors: [{ value: '', msg: 'การลบไม่ถูกต้อง', param: '', location: '' }]}
              req.session.errors=errorss;
              req.session.success=false;
            }else {
                req.session.success=true;
                req.session.topic="ลบข้อมูลเสร็จแล้ว";
            }
            console.log(tribetype);
            res.redirect('/tribe');
        });
    });
};
controller.edit = (req,res) => {
    const { id } = req.params;
        req.getConnection((err,conn)=>{
                conn.query('SELECT * FROM tribe WHERE id= ?',[id],(err,tribetype)=>{
                        res.render('tribe/tribeUpdate',{
                          session: req.session,
                          data1:tribetype[0],
                        });
                });
            });
}
controller.update = (req,res) => {
    const errors = validationResult(req);
    const { id } = req.params;
    const data = req.body;
        if(!errors.isEmpty()){
            req.session.errors=errors;
            req.session.success=false;
            req.getConnection((err,conn)=>{
                conn.query('SELECT * FROM tribe WHERE id= ?',[id],(err,tribetype)=>{
                res.render('tribe/tribeUpdate',{
                  session: req.session,
                  data1:tribetype[0],
                });
              });
            });
        }else{
            req.session.success=true;
            req.session.topic="แก้ไขข้อมูลเสร็จแล้ว";
            req.getConnection((err,conn) => {
                conn.query('UPDATE  tribe SET ?  WHERE id = ?',[data,id],(err,tribetype) => {
                  if(err){
                      res.json(err);
                  }
               res.redirect('/tribe');
               });
             });
        }
};
controller.add = (req,res) => {
    req.getConnection((err,conn) => {
      conn.query('SELECT * FROM tribe',(err,tribetype) =>{
      res.render('tribe/tribeAdd',{
        data:tribetype,
        session: req.session
      });
    });
  });
};
module.exports = controller;
