const controller ={};
const { validationResult } = require('express-validator');

controller.list = (req,res) => {
    req.getConnection((err,conn) =>{
        conn.query('select * from status;',(err,statustype) =>{
            if(err){
                res.json(err);
            }
            res.render('status/statusList',{session: req.session,
              data:statustype,
              });
            });
        });
};
controller.save = (req,res) => {
    const data=req.body;
       const errors = validationResult(req);
        if(!errors.isEmpty()){
            req.session.errors=errors;
            req.session.success=false;
            res.redirect('/status/add');
        }else{
            req.session.success=true;
            req.session.topic="เพิ่มข้อมูลเสร็จแล้ว";
            req.getConnection((err,conn)=>{
            conn.query('INSERT INTO status set ?',[data],(err,statustype)=>{
            res.redirect('/status');
            });
        });
    };
};
controller.del = (req,res) => {
    const { id } = req.params;
    req.getConnection((err,conn)=>{
        conn.query('select * from status HAVING id = ?',[id],(err,statustype)=>{
          if(err){
              res.json(err);
          }
          res.render('status/statusDelete',{
            session: req.session,
            data:statustype[0]
          });
      });
  });
};
controller.delete = (req,res) => {
    const { id } = req.params;
    req.getConnection((err,conn)=>{
        conn.query('DELETE FROM status WHERE id= ?',[id],(err,statustype)=>{
            if(err){
              const  errorss= {errors: [{ value: '', msg: 'การลบไม่ถูกต้อง', param: '', location: '' }]}
              req.session.errors=errorss;
              req.session.success=false;
            }else {
                req.session.success=true;
                req.session.topic="ลบข้อมูลเสร็จแล้ว";
            }
            console.log(statustype);
            res.redirect('/status');
        });
    });
};
controller.edit = (req,res) => {
    const { id } = req.params;
        req.getConnection((err,conn)=>{
                conn.query('SELECT * FROM status WHERE id= ?',[id],(err,statustype)=>{
                        res.render('status/statusUpdate',{
                          session: req.session,
                          data1:statustype[0],
                        });
                });
            });
}
controller.update = (req,res) => {
    const errors = validationResult(req);
    const { id } = req.params;
    const data = req.body;
        if(!errors.isEmpty()){
            req.session.errors=errors;
            req.session.success=false;
            req.getConnection((err,conn)=>{
                conn.query('SELECT * FROM status WHERE id= ?',[id],(err,statustype)=>{
                res.render('status/statusUpdate',{
                  session: req.session,
                  data1:statustype[0],
                });
              });
            });
        }else{
            req.session.success=true;
            req.session.topic="แก้ไขข้อมูลเสร็จแล้ว";
            req.getConnection((err,conn) => {
                conn.query('UPDATE  status SET ?  WHERE id = ?',[data,id],(err,statustype) => {
                  if(err){
                      res.json(err);
                  }
               res.redirect('/status');
               });
             });
        }
};
controller.add = (req,res) => {
    req.getConnection((err,conn) => {
      conn.query('SELECT * FROM status',(err,statustype) =>{
      res.render('status/statusAdd',{
        data:statustype,
        session: req.session
      });
    });
  });
};
module.exports = controller;
