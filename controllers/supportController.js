const controller = {};
const {
  validationResult
} = require('express-validator');

controller.list = (req, res) => {
  req.getConnection((err, conn) => {
    conn.query('select * from support;', (err, supporttype) => {
      if (err) {
        res.json(err);
      }
      res.render('support/supportList', {
        session: req.session,
        data: supporttype,
      });
    });
  });
};
controller.save = (req, res) => {
  const data = req.body;
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    req.session.errors = errors;
    req.session.success = false;
    res.redirect('/support/add');
  } else {
    req.session.success = true;
    req.session.topic = "เพิ่มข้อมูลเสร็จแล้ว";
    req.getConnection((err, conn) => {
      conn.query('INSERT INTO support set ?', [data], (err, supporttype) => {
        res.redirect('/support');
      });
    });
  };
};
controller.del = (req, res) => {
  const {
    id
  } = req.params;
  req.getConnection((err, conn) => {
    conn.query('select * from support HAVING id = ?', [id], (err, supporttype) => {
      if (err) {
        res.json(err);
      }
      res.render('support/supportDelete', {
        session: req.session,
        data: supporttype[0]
      });
    });
  });
};
controller.delete = (req, res) => {
  const {
    id
  } = req.params;
  req.getConnection((err, conn) => {
    conn.query('DELETE FROM support WHERE id= ?', [id], (err, supporttype) => {
      if (err) {
        const errorss = {
          errors: [{
            value: '',
            msg: 'การลบไม่ถูกต้อง',
            param: '',
            location: ''
          }]
        }
        req.session.errors = errorss;
        req.session.success = false;
      } else {
        req.session.success = true;
        req.session.topic = "ลบข้อมูลเสร็จแล้ว";
      }
      console.log(supporttype);
      res.redirect('/support');
    });
  });
};
controller.edit = (req, res) => {
  const {
    id
  } = req.params;
  req.getConnection((err, conn) => {
    conn.query('SELECT * FROM support WHERE id= ?', [id], (err, supporttype) => {
      res.render('support/supportUpdate', {
        session: req.session,
        data1: supporttype[0],
      });
    });
  });
}
controller.update = (req, res) => {
  const errors = validationResult(req);
  const {
    id
  } = req.params;
  const data = req.body;
  if (!errors.isEmpty()) {
    req.session.errors = errors;
    req.session.success = false;
    req.getConnection((err, conn) => {
      conn.query('SELECT * FROM support WHERE id= ?', [id], (err, supporttype) => {
        res.render('support/supportUpdate', {
          session: req.session,
          data1: supporttype[0],
        });
      });
    });
  } else {
    req.session.success = true;
    req.session.topic = "แก้ไขข้อมูลเสร็จแล้ว";
    req.getConnection((err, conn) => {
      conn.query('UPDATE  support SET ?  WHERE id = ?', [data, id], (err, supporttype) => {

        if (err) {
          res.json(err);
        }
        res.redirect('/support');
      });
    });
  }
};
controller.add = (req, res) => {
  req.getConnection((err, conn) => {
    conn.query('SELECT * FROM support', (err, supporttype) => {
      res.render('support/supportAdd', {
        data: supporttype,
        session: req.session
      });
    });
  });
};
module.exports = controller;
