const controller = {};
const {
  validationResult
} = require('express-validator');
const request = require('request');

controller.list = (req, res) => {
  req.getConnection((err, conn) => {

    conn.query('SELECT DATE_FORMAT(now(),"%Y-%m-%d") as datetime', (err, dateday) => {
      conn.query('SELECT DATE_FORMAT(? , "%d")d1,DATE_FORMAT(? , "%m")m2,DATE_FORMAT(DATE_ADD(?,INTERVAL 543 year) , "%Y")y2,DATE_FORMAT(DATE_SUB(?,INTERVAL 1 month) , "%m")m1,DATE_FORMAT(DATE_SUB(DATE_ADD(?,INTERVAL 543 year),INTERVAL 1 month) , "%Y")y1;', [dateday[0].datetime, dateday[0].datetime, dateday[0].datetime, dateday[0].datetime, dateday[0].datetime], (err, daytime) => {
        conn.query('select * from building;', (err, bu) => {
          if (daytime[0].d1 <= 5) {
            conn.query('select rm.watermeter as watermeter,rm.elecmeter as elecmeter,rm.eleccost as eleccost,rm.watercost as watercost,ew.id as ewid,ew.id as ewid ,bu.id as idbu, DATE_FORMAT(ew.payday , "%Y-%m-%d")pay,DATE_FORMAT(ew.payday , "%d")payd ,DATE_FORMAT(ew.payday , "%m")paym,DATE_FORMAT(DATE_ADD(ew.payday,INTERVAL 543 year) , "%Y")payy ,bu.name as buname,rm.number as rmnumber, ew.day as day, ew.oldelec as olde , ew.newelec as newe , ew.oldwater as oldw , ew.newwater as neww from room as rm left join building as bu on rm.building_id = bu.id join (select * from elecwater  WHERE day=?"-"? ) as ew on ew.room_id = rm.id order by rm.id ;', [daytime[0].y1, daytime[0].m1], (err, manage) => {

              conn.query('select * from month', (err, month) => {
                if (err) {
                  res.json(err);
                }
                res.render('elec/elecList', {
                  session: req.session,
                  data: manage,
                  bu: bu
                });
              });
            });
          } else {
            conn.query('select rm.watermeter as watermeter,rm.elecmeter as elecmeter,rm.eleccost as eleccost,rm.watercost as watercost,ew.id as ewid,ew.id as ewid ,bu.id as idbu, DATE_FORMAT(ew.payday , "%Y-%m-%d")pay,DATE_FORMAT(ew.payday , "%d")payd ,DATE_FORMAT(ew.payday , "%m")paym,DATE_FORMAT(DATE_ADD(ew.payday,INTERVAL 543 year) , "%Y")payy ,bu.name as buname,rm.number as rmnumber, ew.day as day, ew.oldelec as olde , ew.newelec as newe , ew.oldwater as oldw , ew.newwater as neww from room as rm left join building as bu on rm.building_id = bu.id join (select * from elecwater  WHERE day=?"-"? ) as ew on ew.room_id = rm.id order by rm.id ;', [daytime[0].y2, daytime[0].m2], (err, manage) => {
              conn.query('select * from month', (err, month) => {
                if (err) {
                  res.json(err);
                }
                res.render('elec/elecList', {
                  session: req.session,
                  data: manage,
                  bu: bu
                });
              });
            });
          }

        });
      });
    });
  });
};

controller.unidome = function (req, res) {
  if (req.session.user) {
    res.render('unidomeElec', {
      session: req.session
    });
  } else {
    res.redirect('/');
  }
};

controller.homeelecwater = function (req, res) {
  if (req.session.user) {
    res.render('homeelecwater', {
      session: req.session
    });
  } else {
    res.redirect('/');
  }
};



controller.add = (req, res) => {
  req.getConnection((err, conn) => {
    conn.query('SELECT DATE_FORMAT(now(),"%Y-%m-%d") as datetime', (err, dateday) => {
      conn.query('SELECT DATE_FORMAT(? , "%d")d1,DATE_FORMAT(? , "%m")m2,DATE_FORMAT(DATE_ADD(?,INTERVAL 543 year) , "%Y")y2,DATE_FORMAT(DATE_SUB(?,INTERVAL 1 month) , "%m")m1,DATE_FORMAT(DATE_SUB(DATE_ADD(?,INTERVAL 543 year),INTERVAL 1 month) , "%Y")y1;', [dateday[0].datetime, dateday[0].datetime, dateday[0].datetime, dateday[0].datetime, dateday[0].datetime], (err, daytime) => {
        conn.query('select * from building;', (err, bu) => {
          if (daytime[0].d1 <= 5) {
            conn.query('select ew.day as day,rm.id as rmid,bu.id as idbu,bu.name as buname,rm.number as rmnumber, ew.newelec as newe, ew.newwater as neww from room as rm left join building as bu on rm.building_id = bu.id left join (select * from elecwater  WHERE day=?-? ) as ew on ew.room_id = rm.id ;', [daytime[0].y1, daytime[0].m1], (err, roomtype) => {

              conn.query('select * from month', (err, month) => {
                if (err) {
                  res.json(err);
                }
                console.log('roomtype');
                res.render('elec/elecAdd', {
                  session: req.session,
                  data: roomtype,
                  data1: daytime[0],
                  month: month,
                  bu: bu
                });
              });
              // console.log('roomtype');
            });
          } else {
            conn.query('select ew.day as day,rm.id as rmid,bu.id as idbu,bu.name as buname,rm.number as rmnumber, ew.newelec as newe, ew.newwater as neww from room as rm left join building as bu on rm.building_id = bu.id left join (select * from elecwater  WHERE day=?-? ) as ew on ew.room_id = rm.id ;', [daytime[0].y2, daytime[0].m2], (err, roomtype) => {
              conn.query('select * from month', (err, month) => {
                if (err) {
                  res.json(err);
                }
                console.log('roomtype');
                res.render('elec/elecAdd', {
                  session: req.session,
                  data: roomtype,
                  data1: daytime[0],
                  month: month,
                  bu: bu
                });

              });
            });
          }

        });
      });
    });
  });
};


controller.addC = (req, res) => {
  req.getConnection((err, conn) => {
    conn.query('SELECT DATE_FORMAT(now(),"%Y-%m-%d") as datetime', (err, dateday) => {
      conn.query('SELECT DATE_FORMAT(? , "%d")d1,DATE_FORMAT(? , "%m")m2,DATE_FORMAT(DATE_ADD(?,INTERVAL 543 year) , "%Y")y2,DATE_FORMAT(DATE_SUB(?,INTERVAL 1 month) , "%m")m1,DATE_FORMAT(DATE_SUB(DATE_ADD(?,INTERVAL 543 year),INTERVAL 1 month) , "%Y")y1;', [dateday[0].datetime, dateday[0].datetime, dateday[0].datetime, dateday[0].datetime, dateday[0].datetime], (err, daytime) => {
        conn.query('select * from building;', (err, bu) => {
          if (daytime[0].d1 <= 5) {
            conn.query('select ew.day as day,rm.id as rmid,bu.id as idbu,bu.name as buname,rm.number as rmnumber, ew.newelec as newe, ew.newwater as neww from room as rm left join building as bu on rm.building_id = bu.id left join (select * from elecwater  WHERE day=?-? ) as ew on ew.room_id = rm.id ;', [daytime[0].y1, daytime[0].m1], (err, roomtype) => {

              conn.query('select * from month', (err, month) => {
                if (err) {
                  res.json(err);
                }
                console.log('roomtype');
                res.render('elec/elecAdd', {
                  session: req.session,
                  data: roomtype,
                  data1: daytime[0],
                  month: month,
                  bu: bu
                });
              });
              // console.log('roomtype');
            });
          } else {
            conn.query('select ew.day as day,rm.id as rmid,bu.id as idbu,bu.name as buname,rm.number as rmnumber, ew.newelec as newe, ew.newwater as neww from room as rm left join building as bu on rm.building_id = bu.id left join (select * from elecwater  WHERE day=?-? ) as ew on ew.room_id = rm.id ;', [daytime[0].y2, daytime[0].m2], (err, roomtype) => {
              conn.query('select * from month', (err, month) => {
                if (err) {
                  res.json(err);
                }
                console.log('roomtype');
                res.render('elec/elecAdd', {
                  session: req.session,
                  data: roomtype,
                  data1: daytime[0],
                  month: month,
                  bu: bu
                });

              });
            });
          }

        });
      });
    });
  });
};

controller.add1 = (req, res) => {
  const {
    id
  } = req.params;

  req.getConnection((err, conn) => {
    conn.query('SELECT DATE_FORMAT(now(),"%Y-%m-%d") as datetime', (err, dateday) => {
      conn.query('SELECT DATE_FORMAT(? , "%d")d1,DATE_FORMAT(? , "%m")m2,DATE_FORMAT(DATE_ADD(?,INTERVAL 543 year) , "%Y")y2,DATE_FORMAT(DATE_SUB(?,INTERVAL 1 month) , "%m")m1,DATE_FORMAT(DATE_SUB(DATE_ADD(?,INTERVAL 543 year),INTERVAL 1 month) , "%Y")y1;', [dateday[0].datetime, dateday[0].datetime, dateday[0].datetime, dateday[0].datetime, dateday[0].datetime], (err, daytime) => {
        conn.query('select rm.id as rmid,bu.name as buname,rm.number as rmnumber, ew.newelec as newe, ew.newwater as neww from room as rm left join building as bu on rm.building_id = bu.id left join elecwater as ew on ew.room_id = rm.id where rm.id = ?;', [id], (err, roomtype) => {
          conn.query(' select ew.day as day,rm.id as rmid,bu.name as buname,rm.number as rmnumber, ew.newelec as newe, ew.newwater as neww from room as rm left join building as bu on rm.building_id = bu.id join elecwater as ew on ew.room_id = rm.id where rm.id=? order by day desc limit 1;', [id], (err, oldew) => {
            if (err) {
              res.json(err);
            }
            res.render('elec/elecAdd1', {
              session: req.session,
              data: roomtype[0],
              data1: daytime[0],
              data2: oldew[0]
            });
          });
        });
      });
    });
  });
};

controller.save = (req, res) => {
  const data = req.body;
  // console.log(data);
  // console.log(data.newwater.length);

  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    req.session.errors = errors;
    req.session.success = false;
    res.redirect('/elec/add');
  } else {
    req.session.success = true;
    req.session.topic = "เพิ่มข้อมูลเสร็จแล้ว";
    for (i = 0; i < data.newwater.length; i++) {
      const datenew = {
        day:[data.day[i]],
        newwater: [data.newwater[i]],
        room_id: [data.room_id[i]]
      }
      // console.log(datenew);
      req.getConnection((err, conn) => {
        conn.query('INSERT INTO elecwater set ?', [datenew], (err, statustype) => {
        });
      });
    };
    res.redirect('/elec');
  };
};

controller.edit = (req, res) => {
  const {
    id
  } = req.params;
  req.getConnection((err, conn) => {
    conn.query('select ew.id as eid,ew.day as day, rm.id as rmid,bu.name as buname,rm.number as rmnumber, ew.newelec as newe,ew.oldelec as olde,ew.oldwater as oldw, ew.newwater as neww from room as rm left join building as bu on rm.building_id = bu.id left join elecwater as ew on ew.room_id = rm.id where rm.id = ? order by ew.day desc;', [id], (err, elecwatertype) => {
      conn.query('SELECT * FROM room', (err, buildingtype) => {
        res.render('elec/elecUpdate', {
          session: req.session,
          data1: elecwatertype[0],
          data: buildingtype
        });
      });
    });
  });
}

controller.update = (req, res) => {
  const errors = validationResult(req);
  const {
    id
  } = req.params;
  const data = req.body;
  if (!errors.isEmpty()) {
    req.session.errors = errors;
    req.session.success = false;
    req.getConnection((err, conn) => {
      conn.query('select rm.id as rmid,bu.name as buname,rm.number as rmnumber, ew.newelec as newe, ew.newwater as neww from room as rm left join building as bu on rm.building_id = bu.id left join elecwater as ew on ew.room_id = rm.id where rm.id = ?;', [id], (err, elecwatertype) => {
        conn.query('SELECT * FROM room', (err, roomtype) => {
          res.render('elec/elecUpdate', {
            session: req.session,
            data1: elecwatertype[0],
            data: roomtype
          });
        });
      });
    });
  } else {
    req.session.success = true;
    req.session.topic = "แก้ไขข้อมูลเสร็จแล้ว";
    req.getConnection((err, conn) => {
      conn.query('UPDATE  elecwater SET ?  WHERE id = ?', [data, id], (err, elecwatertype) => {
        if (err) {
          res.json(err);
        }
        res.redirect('/elec/add');
      });
    });
  }
};
controller.pay = (req, res) => {
  const errors = validationResult(req);
  const {
    id
  } = req.params;

  req.session.success = true;
  req.session.topic = "แก้ไขข้อมูลเสร็จแล้ว";
  req.getConnection((err, conn) => {
    conn.query('UPDATE  elecwater SET payday = now()  WHERE id = ?', [id], (err, elecwatertype) => {
      if (err) {
        res.json(err);
      }
      res.redirect('/elec');
    });
  });
};
controller.cpay = (req, res) => {
  console.log("ploy");
  const errors = validationResult(req);
  const {
    id
  } = req.params;

  req.session.success = true;
  req.session.topic = "แก้ไขข้อมูลเสร็จแล้ว";
  req.getConnection((err, conn) => {
    conn.query('UPDATE  elecwater SET payday = NULL  WHERE id = ?', [id], (err, elecwatertype) => {
      if (err) {
        res.json(err);
      }
      res.redirect('/elec');
    });
  });
};
controller.jsreport = (req, res) => {

  const {
    id
  } = req.params;
  req.getConnection((err, conn) => {
    console.log(id)
    conn.query('select (ew.newelec-ew.oldelec) as selec,ROUND((ew.newelec-ew.oldelec)*rm.eleccost,2) as unit,(ew.newwater-ew.oldwater) as selec1,ROUND((ew.newwater-ew.oldwater)*rm.watercost,2) as unit1,(ROUND((ew.newelec-ew.oldelec)*rm.eleccost,2)+rm.elecmeter) +(ROUND((ew.newwater-ew.oldwater)*rm.watercost,2)+rm.watermeter) as unit2 ,bu.name as buname, rm.number as number ,ew.day as day ,ew.oldelec as oelec,ew.newelec as nelec , rm.eleccost as ecost ,rm.elecmeter as emeter,ew.oldwater as owater,ew.newwater as nwater,rm.watercost as wcost,rm.watermeter as wmeter from elecwater as ew left join room as rm on ew.room_id = rm.id left join building as bu on rm.building_id = bu.id ;', [id], (err, ew) => {
      var data = {
        "template": {
          "shortid": "sl_gLr6U7H"
        },
        data: {
          ew
        }
      }
      var option = {
        url: 'http://127.0.0.1:5488/api/report',
        method: 'POST',
        json: data
      }
      request(option).pipe(res);
    });
  });
};

module.exports = controller;
