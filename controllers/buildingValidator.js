const { check } = require('express-validator');

exports.addValidator = [check('name',"กรุณาใส่ชื่อตึก").not().isEmpty(),
                        check('class',"กรุณาใส่ชั้นตึก").not().isEmpty()
];

 exports.editValidator = [check('name',"กรุณาใส่ชื่อตึก").not().isEmpty(),
                         check('class',"กรุณาใส่ชั้นตึก").not().isEmpty()
 ];
