const controller = {};
const { validationResult } = require('express-validator');

//-------------------------------------------------------------------------------------------------------
//ส่วนแสดงข้อมูลคำร้องขอย้ายหอพัก
//-------------------------------------------------------------------------------------------------------
controller.listmovein = (req, res) => {
  req.getConnection((err, conn) => {
    conn.query('select bu.id as buid, mn.id as mnid ,st.*, fa.name as faname, bu.name as buname, rm.number as roomnumber from movein as mn join student as st on mn.student_id = st.id join faculty as fa on st.faculty_id = fa.id  join current as cu on cu.student_id = st.id join building as bu on cu.building_id = bu.id join room as rm on cu.room_id = rm.id where mn.confirm = 0;', (err, movein) => {
      if (err) {
        res.json(err);
      }
      res.render('tanant_in_out/tanant_list_in', {
        session: req.session,
        movein
      });
    });
  });
};

//-------------------------------------------------------------------------------------------------------
//ส่วนแสดงข้อมูลคำร้องขอย้ายหอพัก หลังจาก confirm
//-------------------------------------------------------------------------------------------------------
controller.listmoveinconfirm = (req, res) => {
  req.getConnection((err, conn) => {
    conn.query('select bu.id as buid, mn.id as mnid ,st.*, fa.name as faname, bu.name as buname, rm.number as roomnumber from movein as mn join student as st on mn.student_id = st.id join faculty as fa on st.faculty_id = fa.id  join current as cu on cu.student_id = st.id join building as bu on cu.building_id = bu.id join room as rm on cu.room_id = rm.id where mn.confirm = 1 and mn.datein is null;', (err, movein) => {
      if (err) {
        res.json(err);
      }
      res.render('tanant_in_out/confirmin', {
        session: req.session,
        movein
      });
    });
  });
};
//-------------------------------------------------------------------------------------------------------
//ส่วนกดยืนยันคำร้องย้ายหอพัก
//-------------------------------------------------------------------------------------------------------
controller.updatemovein = (req, res) => {
  const { id } = req.params;
  console.log(id);
  req.getConnection((err, conn) => {
    conn.query('UPDATE movein set confirm = 1  where id = ? ', [id], (err, movein) => {
      if (err) {
        res.json(err);
      }
      res.redirect('/tanantout/list/comin');
    });
  });
};


//-------------------------------------------------------------------------------------------------------
//ส่วนกดยืนยันการย้ายเข้าหอพัก กรณีย้ายหอพัก
//-------------------------------------------------------------------------------------------------------
controller.updatemoveindate = (req, res) => {
  const { id } = req.params;
  console.log(id);
  req.getConnection((err, conn) => {
    conn.query('UPDATE movein set datein = now() where id = ? ', [id], (err, movein) => {
          conn.query(' select building_id,room_id, student_id from movein  where id = ?; ', [id], (err, moveins) => {
            console.log(err);
            conn.query('update current set building_id = ? , room_id = ? where student_id = ?',[moveins[0].building_id,moveins[0].room_id,moveins[0].student_id], (err, current) => {
              console.log(err);
              if (err) {
                res.json(err);
              }
              res.redirect('/');
            });
          })
      //   });
      // });
    });
  });
};

//-------------------------------------------------------------------------------------------------------
//ส่วนยกเลิกข้อมูลคำร้องขอย้ายหอพัก**
//-------------------------------------------------------------------------------------------------------
controller.deletemovein = (req, res) => {
  const { id } = req.params;
  const { buid } = req.params;
  console.log(buid);
  req.getConnection((err, conn) => {
    conn.query('DELETE from movein where id = ?;', [id], (err, movein) => {
      console.log(err);
      if (err) {
        const errorss = { errors: [{ value: '', msg: 'การลบไม่ถูกต้อง', param: '', location: '' }] }
        req.session.errors = errorss;
        req.session.success = false;
      } else {
        req.session.success = true;
        req.session.topic = "ลบข้อมูลเสร็จแล้ว";
      };
      res.redirect('/student/studentlist/' + buid[0]);
    });
  });
};


//-------------------------------------------------------------------------------------------------------
//ส่วนแสดงคำร้องขอย้ายออก
//-------------------------------------------------------------------------------------------------------
controller.listmoveout = (req, res) => {
  req.getConnection((err, conn) => {
    conn.query('select bu.id as buid,mo.id as moid, st.*, fa.name as faname, bu.name as buname, rm.number as roomnumber from moveout as mo join student as st on mo.student_id = st.id join faculty as fa on st.faculty_id = fa.id  join current as cu on cu.student_id = st.id join building as bu on cu.building_id = bu.id join room as rm on cu.room_id = rm.id where mo.confirm = 0;', (err, moveout) => {
      if (err) {
        res.json(err);
      }
      res.render('tanant_in_out/tanant_list_out', {
        session: req.session,
        moveout: moveout
      });
    });
  });
};

//-------------------------------------------------------------------------------------------------------
//ส่วนแสดงข้อมูลคำร้องย้ายออกหอพัก หลังจาก confirm
//-------------------------------------------------------------------------------------------------------
controller.listmoveoutconfirm = (req, res) => {
  req.getConnection((err, conn) => {
    conn.query('select bu.id as buid ,mo.id as moid, st.*, fa.name as faname, bu.name as buname, rm.number as roomnumber from moveout as mo join student as st on mo.student_id = st.id join faculty as fa on st.faculty_id = fa.id  join current as cu on cu.student_id = st.id join building as bu on cu.building_id = bu.id join room as rm on cu.room_id = rm.id where mo.confirm = 1 and mo.dateout is null;', (err, moveout) => {
      if (err) {
        res.json(err);
      }
      res.render('tanant_in_out/confirmout', {
        session: req.session,
        moveout
      });
    });
  });
};

//-------------------------------------------------------------------------------------------------------
//ส่วนกดย้ายออกหอพัก
//-------------------------------------------------------------------------------------------------------
controller.updatemoveout = (req, res) => {
  const { id } = req.params;
  req.getConnection((err, conn) => {
    conn.query('UPDATE moveout set confirm = 1  where id = ? ', [id], (err, moveout) => {
      if (err) {
        res.json(err);
      }
      res.redirect('/tanantout/list/comout');
    });
  });
};

//-------------------------------------------------------------------------------------------------------
//ส่วนกดยืนยันการย้ายออกหอพัก กรณีย้ายหอพัก
//-------------------------------------------------------------------------------------------------------
controller.updatemoveoutdate = (req, res) => {
  const { id } = req.params;
  req.getConnection((err, conn) => {
    conn.query('UPDATE moveout set dateout = now() where id = ? ', [id], (err, moveouts) => {
      conn.query('select * from moveout where id  = ?', [id], (err, moveout) => {
        conn.query('delete from current where student_id = ? ', [moveout[0].student_id], (err, current) => {
          console.log(id);
          console.log(current);
          if (err) {
            res.json(err);
          }
          res.redirect('/home');
        });
      });
    });
  });
};


//-------------------------------------------------------------------------------------------------------
//ส่วนยกเลิกการย้ายออก
//-------------------------------------------------------------------------------------------------------
controller.deletemoveout = (req, res) => {
  const { id } = req.params;
  const { buid } = req.params;
  req.getConnection((err, conn) => {
    conn.query('DELETE from moveout where id = ?;', [id], (err, moveout) => {
      console.log(err);
      if (err) {
        const errorss = { errors: [{ value: '', msg: 'การลบไม่ถูกต้อง', param: '', location: '' }] }
        req.session.errors = errorss;
        req.session.success = false;
      } else {
        req.session.success = true;
        req.session.topic = "ลบข้อมูลเสร็จแล้ว";
      };
      res.redirect('/student/studentlist/'+ buid[0]);
    });
  });
};


module.exports = controller;
