if ($('#divUp img').attr('src') == 'http://100dayscss.com/codepen/upload.svg') {
  $('#divUp div').hide();
}else {
  $('#divUp span:eq(1)').hide();
}

function readURL(input) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();
    reader.onload = function (e) {
      $('#divUp img').attr('src', e.target.result);
      $('#divUp img').attr('style', 'width: 140px;height: 155px;');
      $('#divUp img').attr('class', '');
      $('#divUp input').attr('class', '');
      $('#divUp input').attr('hidden', true);
      $('#divUp span:eq(1)').hide();
    }
    reader.readAsDataURL(input.files[0]); // convert to base64 string
  }
}

$("#imgInp").change(function () {
  $('#divUp div').show();
  readURL(this);
});
$(".close-pic").on('click', function () {
  // blankPic();
  $('#divUp img').attr('src','http://100dayscss.com/codepen/upload.svg');
  $('#divUp img').attr('style', '');
  $('#divUp img').attr('class', 'upload-icon');
  $('#divUp input').attr('type', 'file');
  $('#divUp input').attr('class', 'upload-input');
  $('#divUp input').attr('hidden', false);
  $('#divUp input').attr('value', '');
  $('#divUp span:eq(1)').show();
  $('#divUp div').hide();
  $('#imgInp').val('');
});

// script for input card ID

$(".form-control").keyup(function () {
  if (this.value.length == this.maxLength) {
    $(this).next('.form-control').focus();
  }
});

// ajax-------------------------------------------------------------------------------------------------------------

$( document ).ready(function() {
  // ajax for facuty and branch
  // GET REQUEST
  $("#getFaculty").click(function(event){
    event.preventDefault();
    facultyGet();
  });

  // DO GET
  function facultyGet(){
    var faid = $("#getFaculty").val();
    $.ajax({
      type : "GET",
      url : "/api/faculty/pull/"+faid,
      success: function(result){
        $('#getBranch').empty();
        var branchList = "";
        $('#getBranch').append('<option>เลือกโปรแกรมวิชา</option>');
        $.each(result, function(i, branch){
          $('#getBranch').append('<option value="'+branch.id+'">'+branch.name+'</option>');
        });
        // console.log("Success: ", result);
      },
      error : function(e) {
        $("#getBranch").append("<option>Error</option>");
        // console.log("ERROR: ", e);
      }
    });
  }

  // ajax for amphures datalist

  // GET REQUEST
  $("#getProvinces").change(function(event){
    $('#getAmphures').val('');
    $('#getDistricts').val('');
    $('#getZipcode').val('');
    event.preventDefault();
    amphuresGet();
  });

  // DO GET
  function amphuresGet(){
    var val = $('#getProvinces').val()
    var prid = $('#provincesOption option').filter(function() {
      return this.value == val;
    }).data('id');
    console.log(prid);
    $.ajax({
      type : "GET",
      url : "/api/amphures/pull/"+prid,
      success: function(result){
        $('#amphuresOption').empty();
        var amphuresList = "";
        $.each(result, function(i, amphures){
          $('#amphuresOption').append('<option data-id="'+amphures.id+'" value="'+amphures.name_th+'"></option>');
        });
        // console.log("Success: ", result);
      },
      error : function(e) {
        $("#amphuresOption").append("<option>Error</option>");
        // console.log("ERROR: ", e);
      }
    });
  }

  // ajax for districts datalist

  // GET REQUEST
  $("#getAmphures").change(function(event){
    $('#getDistricts').val('');
    $('#getZipcode').val('');
    event.preventDefault();
    districtsGet();
  });

  // DO GET
  function districtsGet(){
    var val = $('#getAmphures').val()
    var amid = $('#amphuresOption option').filter(function() {
      return this.value == val;
    }).data('id');
    $.ajax({
      type : "GET",
      url : "/api/districts/pull/"+amid,
      success: function(result){
        $('#districtsOption').empty();
        var districtsList = "";
        $.each(result, function(i, districts){
          $('#districtsOption').append('<option data-id="'+districts.id+'" value="'+districts.name_th+'"></option>');
        });
        // console.log("Success: ", result);
      },
      error : function(e) {
        $("#districtsOption").append("<option>Error</option>");
        // console.log("ERROR: ", e);
      }
    });
  }

  // ajax for zip_code

  // GET REQUEST
  $("#getDistricts").change(function(event){
    $('#getZipcode').val('');
    event.preventDefault();
    zipcodeGet();
  });

  // DO GET
  function zipcodeGet(){
    var val = $('#getDistricts').val()
    var diid = $('#districtsOption option').filter(function() {
      return this.value == val;
    }).data('id');
    $.ajax({
      type : "GET",
      url : "/api/zipcode/pull/"+diid,
      success: function(result){
        $('#getZipcode').empty();
        var zipcodeList = "";
        $.each(result, function(i, zipcode){
          $('#getZipcode').val(zipcode.zip_code);
        });
        // console.log("Success: ", result);
      },
      error : function(e) {
        $("#getZipcode").attr("placeholder","เกิดข้อผิดพลาด");
        // console.log("ERROR: ", e);
      }
    });
  }

  // ajax for contact amphures datalist

  // GET REQUEST
  $("#getContactProvinces").change(function(event){
    $('#getContactAmphures').val('');
    $('#getContactDistricts').val('');
    $('#getContactZipcode').val('');
    event.preventDefault();
    contactAmphuresGet();
  });

  // DO GET
  function contactAmphuresGet(){
    var val = $('#getContactProvinces').val()
    var prid = $('#contactProvincesOption option').filter(function() {
      return this.value == val;
    }).data('id');
    console.log(prid);
    $.ajax({
      type : "GET",
      url : "/api/contactamphures/pull/"+prid,
      success: function(result){
        $('#contactAmphuresOption').empty();
        var amphuresList = "";
        $.each(result, function(i, amphures){
          $('#contactAmphuresOption').append('<option data-id="'+amphures.id+'" value="'+amphures.name_th+'"></option>');
        });
        // console.log("Success: ", result);
      },
      error : function(e) {
        $("#contactAmphuresOption").append("<option>Error</option>");
        // console.log("ERROR: ", e);
      }
    });
  }

  // ajax for contact districts datalist

  // GET REQUEST
  $("#getContactAmphures").change(function(event){
    $('#getContactDistricts').val('');
    $('#getContactZipcode').val('');
    event.preventDefault();
    contactDistrictsGet();
  });

  // DO GET
  function contactDistrictsGet(){
    var val = $('#getContactAmphures').val()
    var amid = $('#contactAmphuresOption option').filter(function() {
      return this.value == val;
    }).data('id');
    $.ajax({
      type : "GET",
      url : "/api/contactdistricts/pull/"+amid,
      success: function(result){
        $('#contactDistrictsOption').empty();
        var districtsList = "";
        $.each(result, function(i, districts){
          $('#contactDistrictsOption').append('<option data-id="'+districts.id+'" value="'+districts.name_th+'"></option>');
        });
        // console.log("Success: ", result);
      },
      error : function(e) {
        $("#contactDistrictsOption").append("<option>Error</option>");
        // console.log("ERROR: ", e);
      }
    });
  }

  // ajax for contact zip_code

  // GET REQUEST
  $("#getContactDistricts").change(function(event){
    $('#getContactZipcode').val('');
    event.preventDefault();
    contactzZipcodeGet();
  });

  // DO GET
  function contactzZipcodeGet(){
    var val = $('#getContactDistricts').val()
    var diid = $('#contactDistrictsOption option').filter(function() {
      return this.value == val;
    }).data('id');
    $.ajax({
      type : "GET",
      url : "/api/contactzipcode/pull/"+diid,
      success: function(result){
        $('#getContactZipcode').empty();
        var zipcodeList = "";
        $.each(result, function(i, zipcode){
          $('#getContactZipcode').val(zipcode.zip_code);
        });
        // console.log("Success: ", result);
      },
      error : function(e) {
        $("#getContactZipcode").attr("placeholder","เกิดข้อผิดพลาด");
        // console.log("ERROR: ", e);
      }
    });
  }

});

$('#otherInput').prop('disabled',true);
function checkInput(){
  if ($('#other').prop('checked') == true) {
    $('#otherInput').prop('disabled',false);
  }else {
    $('#otherInput').prop('disabled',true);
  }
}


$('#parentinput').prop('disabled',true);
function checkparent(){
  if ($('#parent').prop('checked') == true) {
    $('#parentinput').prop('disabled',false);
  }else {
    $('#parentinput').prop('disabled',true);
  }
}
