$(document).ready(function(){
    // ajax for contact zip_code
  
    // GET REQUEST
    $("#getBuilding").click(function(event){
      $('#getRoom').val('');
      event.preventDefault();
      buildingGet();
    });
  
    // DO GET
    function buildingGet(){
      var buid = $('#getBuilding').val()
      $.ajax({
        type : "GET",
        url : "/api/room/pull/"+buid,
        success: function(result){
          $('#getRoom').empty();
          var roomList = "";
          $('#getRoom').append('<option>เลือกห้อง</option>');
          $.each(result, function(i, room){
            $('#getRoom').append('<option value="'+room.id+'">'+room.number+'</option>');
          });
          // console.log("Success: ", result);
        },
        error : function(e) {
          $('#getRoom').empty();
          $('#getRoom').append('<option>เลือกห้อง</option>');
          $("#getRoom").append('<option>เกิดข้อผิดพลาด</option>');
          // console.log("ERROR: ", e);
        }
      });
    }
  })