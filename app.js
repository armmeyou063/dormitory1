const express = require('express');
const body = require('body-parser');
const cookie = require('cookie-parser');
const session = require('express-session');
const mysql = require('mysql');
const connection = require('express-myconnection')
const upload = require("express-fileupload");
const app = express();

app.use(upload());
app.use(express.static('public'));
app.set('view engine', 'ejs');
app.set('views', 'views');

app.use(body.urlencoded({ extended: true }));
app.use(cookie());
app.use(session({
    secret: '123456',
    resave: true,
    saveUninitialized: true
}));

app.use(connection(mysql,{
  host:'localhost',
  user:'root',
  password:'Passw0rd',
  port:3306,
  database:'apartment'
},'single'));

const facultyRoute = require('./routes/facultyRoute');
app.use('/', facultyRoute);

const branchRoute = require('./routes/branchRoute');
app.use('/', branchRoute);

const bursaryRoute = require('./routes/bursaryRoute');
app.use('/', bursaryRoute);

const tribeRoute = require('./routes/tribeRoute');
app.use('/', tribeRoute);

const statusRoute = require('./routes/statusRoute');
app.use('/', statusRoute);

const supportRoute = require('./routes/supportRoute');
app.use('/', supportRoute);

const familyRoute = require('./routes/familyRoute');
app.use('/', familyRoute);

const studentRoute = require('./routes/studentRoute');
app.use('/', studentRoute);

const buildingRoute = require('./routes/buildingRoute');
app.use('/', buildingRoute);

const roomRoute = require('./routes/roomRoute');
app.use('/', roomRoute);

const tanantRoute = require('./routes/tanantRoute');
app.use('/', tanantRoute);

const loginRoute = require('./routes/loginRoute');
app.use('/', loginRoute);

const classnRoute = require('./routes/classnRoute');
app.use('/', classnRoute);

const classnmodeRoute = require('./routes/classnmodeRoute');
app.use('/', classnmodeRoute);

const tribemodeRoute = require('./routes/tribemodeRoute');
app.use('/', tribemodeRoute);

const bursarymodeRoute = require('./routes/bursarymodeRoute');
app.use('/', bursarymodeRoute);

const facultymodeRoute = require('./routes/facultymodeRoute');
app.use('/', facultymodeRoute);

const tanantoutRoute = require('./routes/tanantoutRoute');
app.use('/', tanantoutRoute);

const tanantroomRoute = require('./routes/tanantroomRoute');
app.use('/', tanantroomRoute);

const elecRoute = require('./routes/elecRoute');
app.use('/', elecRoute);

const weRoute = require('./routes/weRoute');
app.use('/', weRoute);

const historyRoute = require('./routes/historyRoute');
app.use('/', historyRoute);

const repairRoute = require('./routes/repairRoute');
app.use('/', repairRoute);

const paydormRoute = require('./routes/paydormRoute');
app.use('/', paydormRoute);

app.listen('8081');
