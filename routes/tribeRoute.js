

const express = require('express');
const router = express.Router();
const tribe = require('../controllers/tribeController');
const validator = require('../controllers/tribeValidator');


router.get('/tribe',tribe.list);
router.post('/tribe/save',tribe.save);
router.get('/tribe/delete/:id',tribe.delete);
router.get('/tribe/del/:id',tribe.del);
router.get('/tribe/update/:id',tribe.edit);
router.post('/tribe/update/:id',tribe.update);
router.get('/tribe/add',tribe.add);


module.exports = router;
