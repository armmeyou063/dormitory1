

const express = require('express');
const router = express.Router();
const bursarymode = require('../controllers/bursarymodeController');
//const validator = require('../controllers/tribeValidator');


router.get('/bursarymode',bursarymode.list);
router.get('/bursarymode/report/:id',bursarymode.report);

module.exports = router;
