

const express = require('express');
const router = express.Router();
const support = require('../controllers/supportController');
const validator = require('../controllers/supportValidator');


router.get('/support',support.list);
router.post('/support/save',support.save);
router.get('/support/delete/:id',support.delete);
router.get('/support/del/:id',support.del);
router.get('/support/update/:id',support.edit);
router.post('/support/update/:id',support.update);
router.get('/support/add',support.add);
module.exports = router;
