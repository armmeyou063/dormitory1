

const express = require('express');
const router = express.Router();
const room = require('../controllers/roomController');
const validator = require('../controllers/roomValidator');


router.get('/room/list/:buid',room.list);
router.get('/room/add/:buid',room.add);
router.post('/room/save/:buid',validator.addValidator,room.save);
router.get('/room/delete/:buid/:roid',room.delete);
router.get('/room/del/:buid/:roid',room.del);
router.get('/room/edit/:buid/:roid',room.edit);
router.post('/room/update/:buid/:roid',validator.editValidator,room.update);

module.exports = router;
