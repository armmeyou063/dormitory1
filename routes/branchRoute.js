

const express = require('express');
const router = express.Router();
const branch = require('../controllers/branchController');
const validator = require('../controllers/branchValidator');


router.get('/branch',branch.list);
router.post('/branch/save',branch.save);
router.get('/branch/delete/:id',branch.delete);
router.get('/branch/del/:id',branch.del);
router.get('/branch/update/:id',branch.edit);
router.post('/branch/update/:id',branch.update);
router.get('/branch/add',branch.add);
module.exports = router;
