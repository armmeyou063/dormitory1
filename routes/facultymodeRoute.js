

const express = require('express');
const router = express.Router();
const facultymode = require('../controllers/facultymodeController');
//const validator = require('../controllers/tribeValidator');


router.get('/facultymode',facultymode.list);
router.get('/facultymode/report/:id',facultymode.report);

module.exports = router;
