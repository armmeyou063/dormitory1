

const express = require('express');
const router = express.Router();
const bursary = require('../controllers/bursaryController');
const validator = require('../controllers/bursaryValidator');


router.get('/bursary',bursary.list);
router.post('/bursary/save',bursary.save);
router.get('/bursary/delete/:id',bursary.delete);
router.get('/bursary/del/:id',bursary.del);
router.get('/bursary/update/:id',bursary.edit);
router.post('/bursary/update/:id',bursary.update);
router.get('/bursary/add',bursary.add);
module.exports = router;
