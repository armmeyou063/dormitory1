const express = require('express');
const router = express.Router();
const paydorm = require('../controllers/paydormController');
const validator = require('../controllers/roomValidator');


router.get('/paydorm/bill',paydorm.paydorm_bill);
router.get('/paydorm/build/:billid',paydorm.paydorm_build);
router.get('/paydorm/room/:buid/:billid',paydorm.paydorm_room);
router.get('/paydorm/student/:buid/:rmid/:billid',paydorm.paydorm_student);
router.get('/paydorm/add/:rmid/:stid/:billid',paydorm.paydorm_add);
router.get('/paydorm/add/:rmid/:billid',paydorm.paydorm_add);
router.post('/paydorm/save/:rmid/:stid/:billid',paydorm.paydorm_save);
module.exports = router;
