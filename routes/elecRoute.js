const express = require('express');
const router = express.Router();
const elec = require('../controllers/elecController');
const validator = require('../controllers/elecValidator');


router.get('/elec',elec.list);
// router.get('/unidomeElec', elec.unidomeElec);
router.get('/homeelecwater', elec.homeelecwater);
//router.get('/elecwater/manage',elecwater.manage);
router.get('/elec/add',elec.add)
// router.get('/waterC/add',water.addC)
router.get('/elec/add1/:id',elec.add1);
router.post('/elec/save',elec.save);
router.get('/elec/update/:id',elec.edit);
router.get('/elec/pay/:id',elec.pay);
router.get('/elec/cpay/:id',elec.cpay);
router.post('/elec/update/:id',elec.update);
router.get('/elec/jsreport',elec.jsreport);


module.exports = router;
