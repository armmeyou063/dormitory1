

const express = require('express');
const router = express.Router();
const building = require('../controllers/buildingController');
const validator = require('../controllers/buildingValidator');


router.get('/building',building.list);
router.post('/building/save',building.save);
router.get('/building/delete/:id',building.delete);
router.get('/building/del/:id',building.del);
router.get('/building/update/:id',building.edit);
router.post('/building/update/:id',building.update);
router.get('/building/add',building.add);
module.exports = router;
