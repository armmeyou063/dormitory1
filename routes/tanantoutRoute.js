const express = require('express');
const router = express.Router();
const tanantout = require('../controllers/tanantoutController');
//const validator = require('../controllers/tanantoutValidator');


router.get('/tanantout/movein',tanantout.listmovein);
router.get('/tanantout/moveout',tanantout.listmoveout);

router.get('/tanantout/update/updatemovein/:id',tanantout.updatemovein);
router.get('/tanantout/delete/deletemovein/:id/:buid',tanantout.deletemovein);

router.get('/tanantout/update/updatemoveout/:id',tanantout.updatemoveout);
router.get('/tanantout/delete/deletemoveout/:id/:buid',tanantout.deletemoveout);

//ส่วนของการยืนยัน การย้ายเข้า - ย้ายออก
router.get('/tanantout/list/comin',tanantout.listmoveinconfirm);
router.get('/tanantout/list/comout',tanantout.listmoveoutconfirm);

router.get('/tanantout/moveincomin/:id',tanantout.updatemoveindate);
router.get('/tanantout/moveoutcomout/:id',tanantout.updatemoveoutdate);
module.exports = router;
