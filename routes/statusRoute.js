

const express = require('express');
const router = express.Router();
const status = require('../controllers/statusController');
const validator = require('../controllers/statusValidator');


router.get('/status',status.list);
router.post('/status/save',status.save);
router.get('/status/delete/:id',status.delete);
router.get('/status/del/:id',status.del);
router.get('/status/update/:id',status.edit);
router.post('/status/update/:id',status.update);
router.get('/status/add',status.add);
module.exports = router;
