

const express = require('express');
const router = express.Router();
const family = require('../controllers/familyController');
//const validator = require('../controllers/familyValidator');


router.get('/family',family.list);

router.get('/family/report/:id',family.report);

module.exports = router;
