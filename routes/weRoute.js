const express = require('express');
const router = express.Router();
const we = require('../controllers/weController');
const validator = require('../controllers/weValidator');


router.get('/we/build',we.water_build);

router.get('/we/list/:unid',we.list);
router.post('/we/list/:unid',we.list);

router.get('/we/add/:day/:unid',we.add)
router.post('/we/save/:day/:unid',we.save);
router.get('/we/edit/:day/:unid',we.edit);
router.post('/we/update/:day/:unid',we.update);

router.get('/we/unidome', we.unidome);
router.get('/we/pay/:unid/:weid',we.pay);
router.get('/we/cpay/:unid/:weid',we.cpay);
router.get('/we/jsreport',we.jsreport);

module.exports = router;
