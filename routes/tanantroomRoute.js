


const express = require('express');
const router = express.Router();
const tanantroom = require('../controllers/tanantroomController');
//const validator = require('../controllers/tribeValidator');


router.get('/tanantroom',tanantroom.list);
router.get('/tanantroom/report/:stid',tanantroom.report);
router.get('/tanantroom/promise/:stid',tanantroom.promise);
router.post('/tanantroom/savepromise/:stid',tanantroom.promise_save);
router.get('/tanantcomin',tanantroom.jonglist);
router.get('/tanantroom/updatejong/:id',tanantroom.updatejong);
router.get('/tanantroom/updatecomin/:tnid/:buid',tanantroom.updatecomin);
router.get('/tanantroom/delete/:id/:stid',tanantroom.delete);
router.get('/tanantroom/del/:id',tanantroom.del);


module.exports = router;
