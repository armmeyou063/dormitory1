const express = require('express');
const router = express.Router();
const loginController = require('../controllers/loginController');

//ส่วนของการส่งข้อมูล
router.get('/',loginController.Dorm);
router.post('/',loginController.Dorm);
router.get('/room/:buid',loginController.Room);
router.get('/index/:buid/:rmid',loginController.index);
router.post('/save/:buid/:rmid',loginController.save);

//ส่วนของการส่งข้อมูลหน้าล็อคอิน
router.get('/login', loginController.log);
router.get('/home', loginController.home);
router.post('/home', loginController.login);
router.get('/logout', loginController.logout);

// route for ajax
router.get('/api/faculty/pull/:faid',loginController.getbranch);

router.get('/api/amphures/pull/:prid',loginController.getamphures);
router.get('/api/districts/pull/:amid',loginController.getdistricts);
router.get('/api/zipcode/pull/:diid',loginController.getzipcode);

router.get('/api/contactamphures/pull/:prid',loginController.getcontactamphures);
router.get('/api/contactdistricts/pull/:amid',loginController.getcontactdistricts);
router.get('/api/contactzipcode/pull/:diid',loginController.getcontactzipcode);

module.exports = router;
