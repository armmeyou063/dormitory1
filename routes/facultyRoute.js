

const express = require('express');
const router = express.Router();
const faculty = require('../controllers/facultyController');
const validator = require('../controllers/facultyValidator');


router.get('/faculty',faculty.list);
router.post('/faculty/save',faculty.save);
router.get('/faculty/delete/:id',faculty.delete);
router.get('/faculty/del/:id',faculty.del);
router.get('/faculty/update/:id',faculty.edit);
router.post('/faculty/update/:id',faculty.update);
router.get('/faculty/add',faculty.add);

module.exports = router;
