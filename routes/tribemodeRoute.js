

const express = require('express');
const router = express.Router();
const tribemode = require('../controllers/tribemodeController');
//const validator = require('../controllers/tribeValidator');


router.get('/tribemode',tribemode.list);
router.get('/tribemode/report/:id',tribemode.report);
router.get('/tribemode/jsreport',tribemode.jsreport);

module.exports = router;
