const express = require('express');
const router = express.Router();
const repair = require('../controllers/repairController');
const validator = require('../controllers/roomValidator');


router.get('/repair/add',repair.repair_add);
router.post('/repair/save',repair.repair_save);
router.get('/repair/confirm',repair.repair_confirm);
router.get('/repair/confirm/:id',repair.repair_saveconfirm);
router.get('/repair/date',repair.repair_date);
router.get('/repair/date/:id',repair.repair_savedate);
module.exports = router;
