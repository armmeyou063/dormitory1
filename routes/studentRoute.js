const express = require('express');
const router = express.Router();
const student = require('../controllers/studentController');
const validator = require('../controllers/studentValidator');

//เลือกอาคาร
router.get('/student/unidorm',student.unidorm);
//แสดงรายชื่อ นศ
router.get('/student/studentlist/:buid',student.studentlist);
//หน้าแรก dorm
router.get('/student/studentmovein',student.addmovein);
router.get('/student/studentmoveout',student.addmoveout);
//หน้าหลังจาก login
router.get('/student/movein/:buid/:stid',student.addmovein);
router.get('/student/moveout/:buid/:stid',student.addmoveout);
//หน้าแรก เมนู ย้ายออก  ย้ายหอพัก
router.post('/studentmovein/add',student.moveinadd);
router.post('/studentmoveout/add',student.moveoutadd);
//ส่วนบันทึกข้อมูล
router.post('/student/movein/savein/:stid',student.savemovein);
router.post('/student/moveout/saveout/:stid',student.savemoveout);

router.get('/studentmoveout/delete/:id',student.deletemovein);



///////////////ไม่เกี่ยว/////////////////////////
router.get('/student/save/:id',student.save);
router.post('/student/save',student.save);
router.post('/student/save/:id',student.save);
router.get('/student/editRoom/:idrm/:idbu/:idnump/:idst/:strm_id/:numrm',student.editRoom);
router.get('/student/delete/:id',student.delete);
router.get('/student/del/:id',student.del);
router.get('/student/update/:id',student.edit);
router.post('/student/update/:id',student.update);
router.get('/student/add/:idrm/:idbu',student.add);
router.get('/student/add/:idrm/:idbu/:tanantroomid',student.add);
router.get('/student/report/:id',student.report);

// route for ajax

router.get('/api/room/pull/:buid',student.getroom);

module.exports = router;
