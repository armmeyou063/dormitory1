

const express = require('express');
const router = express.Router();
const classnmode = require('../controllers/classnmodeController');
//const validator = require('../controllers/tribeValidator');


router.get('/classnmode',classnmode.list);
router.get('/classnmode/report/:id',classnmode.report);
router.get('/classnmode/jsreport',classnmode.jsreport);

module.exports = router;
