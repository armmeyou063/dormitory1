const express = require('express');
const router = express.Router();
const tanant = require('../controllers/tanantController');
//const validator = require('../controllers/tanantValidator');


router.get('/tanant',tanant.list);
router.post('/tanant/delete/:id/:strm_id/:numrm',tanant.delete);
router.get('/tanant/del/:id/:strm_id/:numrm',tanant.del);
router.get('/tanant/listTanantBuilding',tanant.listTanantBuilding);
router.get('/tanant/listTanantBuilding/:id',tanant.listTanantBuilding);

router.get('/tanant/ListBuilding',tanant.ListBuilding);
router.get('/tanant/ListBuildingSee/:id',tanant.ListBuildingSee);

router.get('/tanant/addBuilding/:id',tanant.addBuilding);
router.get('/tanant/addBuilding/:id/:tanantroomid',tanant.addBuilding);
router.get('/tanant/EditBuilding/:idst/:strm_id/:numrm',tanant.ListEditBuilding);
router.get('/tanant/addEditBuilding/:id/:idst/:strm_id/:numrm',tanant.addEditBuilding);
module.exports = router;
